from setuptools import find_packages
from numpy.distutils.core import setup, Extension

ext = [
        Extension(name='resource_dependency', 
            sources=[
                     'src/fortran/sorting.f95', 
                     'src/fortran/random.f95', 
                     'src/fortran/topology.f95', 
                     'src/fortran/auxiliary.f95', 
                     'src/fortran/delete_vertex.f95',
                     'src/fortran/resource_dependency.f95', 
                     'src/C/prob_dists.c', 
                     'src/C/init_rng.c',
                    ],
            libraries=['gsl', 'gslcblas']
                ),
        Extension(name='request_offer', 
            sources=[
                     'src/fortran/sorting.f95', 
                     'src/fortran/random.f95', 
                     'src/fortran/topology.f95', 
                     'src/fortran/auxiliary.f95', 
                     'src/fortran/delete_vertex.f95',
                     'src/fortran/request_offer_model.f95',
                     'src/C/prob_dists.c',
                     'src/C/init_rng.c',
                    ], 
            libraries=['gsl', 'gslcblas']
                 ),
    ]

setup(
    name='dependency-networks',
    version='5.0.0',
    author='Snehal M. Shekatkar',
    author_email='snehal@inferred.in',
    long_description=open('README.md').read(),
    package_dir={'': 'src'},
    packages=find_packages(where='src'),
    install_requires=['numpy', ],
    classifiers=[
        'License :: OSI Approved :: GNU General Public License v3 or later (GPLv3+)',
        'Programming Language :: Python :: 3', 
    ],
    ext_modules=ext,
    python_requires='>=3.6', 
)

