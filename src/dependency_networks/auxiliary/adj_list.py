#    This file is part of the package dependency-networks
#
#    Copyright (C) 2020 Snehal Madhukar Shekatkar <snehal@inferred.in>
#
#    This package is free software: you can redistribute it and/or modify
#    it under the terms of the GNU General Public License as published by
#    the Free Software Foundation, either version 3 of the License, or
#    (at your option) any later version.
#
#    The package is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
#    GNU General Public License for more details.
#
#    You should have received a copy of the GNU General Public License
#    along with this package. If not, see <https://www.gnu.org/licenses/>.

import numpy as np

def _get_adj_list(g, deg="out", multigraph=True, weighted=False):
    r"""Return an adjacency list and degrees for graph g
    """
    
    try:
        import graph_tool.all as gt
        if isinstance(g, gt.Graph):
            n = g.num_vertices()
            if multigraph == False:
                # Remove parallel edges and self-loops
                gt.remove_parallel_edges(g)
                gt.remove_self_loops(g)
            if deg == "out":
                degs = g.get_out_degrees(range(n))
            else:
                degs = g.get_in_degrees(range(n))
            max_deg = np.max(degs)
            # Initially the adjacency list contains all -2s, and then it is
            # filled while looping over the vertices
            adj_list = -2 * np.ones((max_deg, n), dtype='int')
            if weighted:
                weights_adj_list = -1 * np.ones((max_deg, n), dtype='float')
                if 'weight' in g.edge_properties:
                    weight = g.edge_properties['weight']
                else:
                    weight = g.new_edge_property('float')
                    weight.a = 1.0
            else:
                weights_adj_list = np.ones((max_deg, n), dtype='float')
            for v in g.vertices():
                if deg == "out":
                    nbrs = g.get_out_neighbours(v)
                    adj_list[:v.out_degree(), int(v)] = nbrs
                    if weighted:
                        weights_adj_list[:v.out_degree(), int(v)] = [weight[g.edge(v, nbr)] for nbr in nbrs]
                elif deg == "in":
                    nbrs = g.get_in_neighbours(v)
                    adj_list[:v.in_degree(), int(v)] = nbrs
                    if weighted:
                        weights_adj_list[:v.in_degree(), int(v)] = [weight[g.edge(v, nbr)] for nbr in nbrs]
                else:
                    raise ValueError("deg should either be 'in' or 'out'")
            # Make values Fortran compatible
            adj_list += 1 
            return adj_list, weights_adj_list, degs
    except:
        pass

    try:
        import networkx as nx
        def _get_true_neighbours_nx(g, v, deg='out'):
            """Return a list of neighbours of vertex v so that each multi-edge and
            self-loop are properly represented. For an undirected graph, each self-loop
            is represented by adding a vertex to its own neighbour list twice while for
            directed graph, each self-loop is represented by adding a vertex to its own
            neighbour list (in or out) only once. 
            """
            if isinstance(g, nx.MultiDiGraph) or isinstance(g, nx.DiGraph):
                true_nbrs = []
                if deg == 'out':
                    for nbr in g.successors(v):
                        for _ in range(g.number_of_edges(v, nbr)):
                            true_nbrs.append(nbr)
                elif deg == 'in':
                    for nbr in g.predecessors(v):
                        for _ in range(g.number_of_edges(nbr, v)):
                            true_nbrs.append(nbr)
            elif isinstance(g, nx.MultiGraph):
                true_nbrs = []
                for nbr in g.neighbors(v):
                    for _ in range(g.number_of_edges(v, nbr)):
                        if v == nbr:
                            true_nbrs.extend([v, v])
                        else:
                            true_nbrs.append(nbr)
            else:
                true_nbrs = g.neighbors(v)
            return list(true_nbrs)
        
        def _set_simple_nx(g):
            """Convert a multigraph to a simple graph by collapsing each multiedge
            to a single edge, and by removing all self-loops    
            """
            edges = set(g.edges()) 
            if isinstance(g, nx.MultiDiGraph):
                g = nx.DiGraph()
                g.add_edges_from(edges)
            elif isinstance(g, nx.MultiGraph):
                g = nx.Graph()
                g.add_edges_from(edges)
            # Remove self-loops
            for v in nx.nodes_with_selfloops(g): 
                g.remove_edge(v, v)
            return g

        if isinstance(g, nx.Graph):
            # Remove all the self-loops and collapse the parallel edges to single edges if 
            # eighter multigraph == False or the graph is not an instance of nx.MultiGraph or
            # of nx.MultiDiGraph
            if multigraph == False or (not isinstance(g, nx.MultiGraph) and not isinstance(g, nx.MultiDiGraph)):
                g = _set_simple_nx(g)

            vertices = g.nodes()
            if isinstance(g, nx.MultiDiGraph) or isinstance(g, nx.DiGraph):
                if deg == 'out':
                    degs = np.array(list(g.out_degree()))[:, 1]
                elif deg == 'in':
                    degs = np.array(list(g.in_degree()))[:, 1]

                max_deg = degs.max()
                adj_list = -2 * np.ones((max_deg, g.number_of_nodes()), dtype="int")
                if weighted:
                    weights_adj_list = -2 * np.ones((max_deg, g.number_of_nodes()))
                    if not nx.is_weighted(g):
                        weights_adj_list = np.ones((max_deg, g.number_of_nodes()), dtype='float')
                else:
                    weights_adj_list = np.ones((max_deg, g.number_of_nodes()), dtype='float')
                for v in g.nodes():
                    if deg == 'out':
                        nbrs = _get_true_neighbours_nx(g, v, deg='out')
                        adj_list[:g.out_degree(v), v] = nbrs
                        nbrs = list(g.successors(v))
                        if weighted and nx.is_weighted(g):
                            if isinstance(g, nx.MultiDiGraph):
                                # Fetch weights of all the out-edges connected to v
                                nbr_out_edge_weights = []
                                for nbr in nbrs:
                                    weight_dict = g.get_edge_data(v, nbr)
                                    for j in weight_dict:
                                        nbr_out_edge_weights.append(weight_dict[j]['weight'])
                                weights_adj_list[:g.out_degree(v), v] = nbr_out_edge_weights
                            elif isinstance(g, nx.DiGraph):
                                weights_adj_list[:g.out_degree(v), v] = [g.edges[v, nbr]['weight'] for nbr in nbrs]
                    elif deg == 'in':
                        nbrs = _get_true_neighbours_nx(g, v, deg='in')
                        adj_list[:g.in_degree(v), v] = nbrs
                        nbrs = list(g.predecessors(v))
                        if weighted and nx.is_weighted(g):
                            if isinstance(g, nx.MultiDiGraph):
                                # Fetch weights of all the in-edges connected to v
                                nbr_in_edge_weights = []
                                for nbr in nbrs:
                                    weight_dict = g.get_edge_data(nbr, v)
                                    for j in weight_dict:
                                        nbr_in_edge_weights.append(weight_dict[j]['weight'])
                                weights_adj_list[:g.in_degree(), v] = nbr_in_edge_weights
                            elif isinstance(g, nx.DiGraph):
                                weights_adj_list[:g.in_degree(v), v] = [g.edges[nbr, v]['weight'] for nbr in nbrs]
            elif isinstance(g, nx.MultiGraph) or isinstance(g, nx.Graph):
                degs = np.array(list(g.degree()))[:, 1]
                max_deg = degs.max()
                adj_list = -2 * np.ones((max_deg, g.number_of_nodes()), dtype="int")
                if weighted:
                    weights_adj_list = -2 * np.ones((max_deg, g.number_of_nodes()), dtype='float')
                    if not nx.is_weighted(g):
                        weights_adj_list = np.ones((max_deg, g.number_of_nodes()), dtype='float')
                else:
                    weights_adj_list = np.ones((max_deg, g.number_of_nodes()), dtype='float')
                for v in g.nodes():
                    nbrs = _get_true_neighbours_nx(g, v)
                    adj_list[:g.degree(v), v] = nbrs
                    nbrs = list(g.neighbors(v))
                    if weighted and nx.is_weighted(g):
                        if isinstance(g, nx.MultiGraph):
                            # Fetch weights of all the edges connected to v
                            nbr_edge_weights = [] 
                            for nbr in nbrs:
                                weight_dict = g.get_edge_data(v, nbr)
                                for j in weight_dict:
                                    nbr_edge_weights.append(weight_dict[j]['weight'])
                                    # Add weight once more if the edge is a self-loop
                                    if v == nbr:
                                        nbr_edge_weights.append(weight_dict[j]['weight'])
                            weights_adj_list[:g.degree(v), v] = nbr_edge_weights
                        elif isinstance(g, nx.Graph):
                            weights_adj_list[:g.degree(v), v] = [g.edges[v, nbr]['weight'] for nbr in nbrs]
                    
            # Make adj_list Fortran compatible
            adj_list += 1
            return adj_list, weights_adj_list, degs
    except:
        pass

def _multi_to_weighted(g):
    try:
        import graph_tool.all as gt
        if isinstance(g, gt.Graph):
            adj_mat = gt.adjacency(g)
            if not 'weight' in g.edge_properties:
                weight = g.new_edge_property('float')
                weight.a = 1.0
                for e in g.edges():
                    u, v = e.source(), e.target()
                    # adj_mat[v, u] = 1 iff u -> v
                    weight[e] = adj_mat[v, u]
                gt.remove_parallel_edges(g)
                gt.remove_self_loops(g)
                # Add back a single self-loop with a correct weight
                for v in g.vertices():
                    if adj_mat[v, v] > 0:
                        e = g.add_edge(v, v)
                        # Multiply by 0.5 because every self-edge is represented by 2
                        weight[e] = 0.5*adj_mat[v, v]
                g.ep['weight'] = weight
            else:
                weight = g.edge_properties['weight']
                # Keep track of whether we have already visited a vertex pair or not
                traversed = g.new_edge_property('bool')
                remove = g.new_edge_property('bool')
                for e in g.edges():
                    traversed[e] = False
                    remove[e] = False
                for e in g.edges():
                    if not traversed[e]:
                        u, v = e.source(), e.target()
                        # Assign the edge weight the sum of the weights of all 
                        # edges between u and v
                        tot_weight = sum([weight[e_uv] for e_uv in g.edge(u, v, all_edges=True)])
                        if u != v:
                            weight[e] = tot_weight
                        else:
                            weight[e] = 0.5 * tot_weight
                        traversed[e] = True
                        for e_uv in g.edge(u, v, all_edges=True):
                            if traversed[e_uv] == False:
                                remove[e_uv] = True
                for e in g.edges():
                    if remove[e]:
                        g.remove_edge(e)
            return g
    except:
        pass
