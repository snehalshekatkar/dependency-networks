#    This file is part of the package dependency-networks
#
#    Copyright (C) 2020 Snehal Madhukar Shekatkar <snehal@inferred.in>
#
#    This package is free software: you can redistribute it and/or modify
#    it under the terms of the GNU General Public License as published by
#    the Free Software Foundation, either version 3 of the License, or
#    (at your option) any later version.
#
#    The package is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
#    GNU General Public License for more details.
#
#    You should have received a copy of the GNU General Public License
#    along with this package. If not, see <https://www.gnu.org/licenses/>.

def _nbr_prop_to_index(prop):
    if prop == "base_resource":
        return 0
    elif prop == "R":
        return 1
    elif prop == "eta":
        return 2
    elif prop == "deg" or prop == "out_deg":
        return 3
    elif prop == "deg_temp" or prop == "out_deg_temp":
        return 4
    elif prop == "in_deg":
        return 5
    elif prop == "in_deg_temp":
        return 6
    elif prop == "external":
        return 7
    elif prop == "mu" or prop == "beta" or prop == "a" or prop == "alpha":
        return 8
    elif prop == "sigma" or prop == "b" or prop == "xm":
        return 9
    else:
        raise ValueError(f"Vertex property {prop} supplied for masking not understood.")
            
def _condition_to_int(s):
    if s == "<": 
        return 0
    elif s == ">":
        return 1
    elif s == "<=":
        return 2
    elif s == ">=":
        return 3
    elif s == "=":
        return 4
    elif s == "!=":
        return 5
    else:
        raise ValueError(f"Condition {s} not understood.")
