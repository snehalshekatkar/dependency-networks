#    This file is part of the package dependency-networks
#
#    Copyright (C) 2020 Snehal Madhukar Shekatkar <snehal@inferred.in>
#
#    This package is free software: you can redistribute it and/or modify
#    it under the terms of the GNU General Public License as published by
#    the Free Software Foundation, either version 3 of the License, or
#    (at your option) any later version.
#
#    The package is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
#    GNU General Public License for more details.
#
#    You should have received a copy of the GNU General Public License
#    along with this package. If not, see <https://www.gnu.org/licenses/>.

"""
Simulate the resource dependency model described in :
    .. [ingale-resource-2020] Madhusudan Ingale and Snehal M. Shekatkar, 
       "Resource dependency and survivability in complex networks". 
       Physical Review E 102, no. 1: 062304 (2020) 
       :doi:`10.1103/PhysRevE.102.062304`, :arXiv:`2006.07082`
"""

import os
import numpy as np
import resource_dependency as rd
from copy import deepcopy

def network_activity(n, g, multigraph=False, weighted=False, 
    death=False, prob_dist='exponential', dist_params={"beta" : 2.0}, 
    R=1.0, eta=0.0, tot_iter=100, all_verts_mask=True, 
    masking_verts_conds=[], masking_verts_ext_prop_vals=0.0, 
    masking_verts_OR=True, masked_nbrs_conds=[], 
    masked_nbrs_ext_prop_vals=0.0, masked_nbrs_OR=True, tot_groups=1, 
    group_label=None, temporal=False, temporal_lc=False, 
    temporal_resol=1, vertex_time_series=False, 
    temporal_summary_file='temporal.csv', 
    vertex_time_series_file='vert_ts.csv', vertex_data=False, 
    vertex_file='vertex.csv', seed=None):
    r"""
    Simulate the network resource activity using resource dependency model.

    Parameters
    ----------
    n : int 
        number of vertices in the original graph.
    g : :class:`graph_tool.Graph` or :class:`networkx.Graph` or :class:`numpy.ndarray` or list
        The network for which to simulate the death. This could either 
        be an instance of the :class:`graph_tool.Graph` or 
        :class:`networkx.Graph`. For a directed graph, an edge from 
        ``i`` to ``j`` depicts the flow of resources from  ``i`` to 
        ``j``.  Whatever the type of input, the vertices in the graph 
        must be labeled as ``0, 1, ..., n-1``.
    multigraph : bool (optional, default: ``False``)
        Whether to consider the graph as a multi-graph or not. If 
        ``False``, all multi-edges between distinct vertices are 
        collapsed to a single edge, while all self-loops are removed. 
        If the graph is a networkx graph, it must have a correct type 
        (nx.MultiGraph or nx.MultiDiGraph).
    weighted : bool (optional, default: ``False``)
        Whether to take into account edge weights which must be provided        
        in the form of an internal property map for a graph-tool graph 
        or in the form of an edge attribute for a networkx graph.
    death : bool (optional, default: ``False``)
        Whether a vertex dies when total resouce on it becomes less 
        than its thershold.
    prob_dist : str (optional, default: ``"exponential"``)
        The probability distribution to be used to generate resource at
        each vertex. Available distributions are:

        ``uniform``
            The uniform distribution over :math:`(a, b)`.
        ``exponential``
            The Exponential distribution :math:`Exp(\beta)` with probability density: 

            .. math::
                p(x, \beta) = \frac{1}{\beta}e^{-\frac{x}{\beta}}
        ``gaussian``
            The Gaussian/Normal distribution :math:`\mathcal{N}(\mu, \sigma^2)` with
            mean :math:`\mu` and standard deviation :math:`sigma`.
        ``gaussian_positive``
            Modified Gaussian/Normal distribution obtained by discarding the negative 
            tail of the usual Gaussian distribution :math:`\mathcal{N}(\mu, \sigma^2)` 
            with mean :math:`\mu` and standard deviation :math:`sigma`.
        ``pareto``
            The Pareto distribution with probability density:

            .. math::
                p(x, \alpha, x_m) = \frac{\alpha x_m^\alpha}{x^{\alpha+1}}\quad \text{for}\ x \ge x_m
        ``chi``
            The Chi distribution with `dof` degrees of freedom.
        ``chi-squared``
            The Chi-squared distribution with `dof` degrees of freedom.
        ``logistic``
            The Logistic distribution with location parameter :math:`\mu` and scale parameter :math:`a`.
        ``logistic_positive``
            Modified Logistic distribution obtained by discarding the negative tail
            of the usual Logistic distribution with location parameter :math:`\mu` and 
            scale parameter :math:`a`.
        ``gamma``
            The Gamma distribution with shape parameter `a` and scale parameter :math:`beta`.
            
    dist_params : dict (optional, default: ``{"beta" : 2.0}``)
        Parameters of the probability distribution ``prob_dist`` being used. 
        An array of length ``n`` can be passed for each parameter so as to
        have different values for different vertices. The parameters for 
        different distributions are:

        ``uniform``           : ``a``, ``b``
        ``exponential``       : ``beta``
        ``gaussian``          : ``mu``, ``sigma``
        ``gaussian_positive`` : ``mu``, ``sigma``
        ``pareto``            : ``alpha``, ``xm``
        ``chi``               : ``dof``
        ``chi-squared``       : ``dof``
        ``logistic``          : ``mu``, ``a``
        ``logistic_positive`` : ``mu``, ``a``
        ``gamma``             : ``a``, ``beta``

    R : float or :class:`numpy.ndarray` (optional, default: ``1.0``)        
        The survival threshold for a vertex. If the total amount of resource
        is less than R, the vertex dies immediately. 
    eta : float or :class:`numpy.ndarray` (optional, default: ``0.0``)
        Distribution bias. The part of the surplus amount that the neighbour 
        :math:`j` receives is proportional to :math:`1/k_j^{\eta}`. For 
        directed networks, :math:`k_j` denotes the in-degree of :math:`j`.
    tot_iter : int (optional, default: ``100``)
        The total number of iterations to perform. This has no effect 
        when ``death == True`` since in that case iterations are 
        performed until the whole network dies.
    all_verts_mask : bool (optional, default: ``True``)
        Whether all vertices try to mask their neighbours or not. 
    masking_verts_conds : list of tuples (optional, default: `[]`)
        Conditions to use when ``all_verts_mask == False`` to decide whether 
        at a given time step a vertex attempts to mask its neighbours or not 
        while distributing the surplus based on the properties of each 
        neighbour. This has to be a list of tuples where each tuple represents 
        one condition, and contains exactly three strings. The first string is 
        the name of the property, the second is a relational operator 
        (one of "<", ">", "<=", ">=", "=", "!="), and the third is the 
        numerical value to be used in the condition.  Supported properties are 
        ``"base_resource"``, ``"R"``, ``"eta"``, ``"deg"``, ``"deg_temp"``, 
        ``"in_deg"``, ``"in_deg_temp"``, ``"external"``, and any of the 
        parameters in ``dist_params``. For a directed graph, ``"deg"`` is same 
        as ``"out_deg"`` and ``"deg_temp"`` is same as temporary out-degree. 
        For the property ``"external"``, an array containing its values must be
        passed using ``masking_verts_ext_prop_vals`` argument. This has effect
        only if ``all_verts_mask == False``. 
    masking_verts_ext_prop_vals : float or :class:`numpy.ndarray` (optional, default: ``0.0``)
        The values of an ``"external"`` property to be used to use to decide 
        which vertices will mask their neighbour. This is used only if one of 
        the properties provided in ``masking_verts_conds`` is ``"external"``.
    masking_verts_OR : bool (optional, default: ``True``)
        When multiple conditions are supplied for ``masking_verts_conds``, 
        whether to use OR operator or AND operator to combine them. If ``True``,
        even if one condition is satisfied, the vertex attempts to mask 
        neighbours depending upon ``masked_nbrs_conds`` below. Otherwise, only 
        if all the conditions are satisfied, the vertex attempts to mask neighbours. 
        This has effect only if ``all_verts_mask == False``.
    masked_nbrs_conds : list of tuples or :class:`numpy.ndarray` (optional, default: [])
        Conditions to use to decide whether a given neighbour of a vertex is 
        to be masked at a given time step or not. For a directed graph, a 
        neighbour means an out-neighbour since the resources from a vertex can 
        only flow to the out-neighbours. Everything else identical to the 
        description for ``masking_verts_conds`` argument above.
    masked_nbrs_ext_prop_vals : float or :class:`numpy.ndarray` (optional, default: ``0.0``)
        The values of an external property that is to be used for masking 
        neighbours. This is used only if one of the properties provided in 
        ``masked_nbrs_conds`` is ``"external"``. 
    masked_nbrs_OR : bool (optional, default: ``True``)
        When multiple conditions are supplied for ``masked_nbrs_conds``, 
        whether to use OR operator or AND operator to combine them. If ``True``,
        even if one condition is satisfied for a neighbour, it is masked.
        Otherwise, only if all the conditions are satisfied, the vertex with 
        surplus masks the neighbour. 
    tot_groups : int (optional, default: ``1``)
        The total number of groups of vertices whose sizes are to be tracked
        as the function of time.
    group_label : :class:`numpy.ndarray`
        Array containing labels of groups. If there are total B groups, then 
        the labels must be ``0, 1, 2, ..., B-1``. 
    temporal : bool (optional, default: ``False``)
        Whether to save the summary of the temporal structure of the network. 
        The summary includes the size of the network, and the average degree of
        the network. If ``temporal_lc==True`` (see below) and the graph is 
        undirected, the summary also includes the size of the largest component
        and its density (till its size is at least :math:`2`). The output is 
        stored in ``temporal_summary_file`` (see below). If there are more 
        than one groups of vertices, the file ``"group_sizes.csv"`` is also
        generated which records the sizes of the groups as a function of time. 
        In the same case, the file ``"group_ave_degs.csv"`` is also generated 
        which contains the average degrees of the individual groups as a 
        function of time. If the graph is directed, then two separate files 
        are created for in and out degrees. 
    temporal_lc : bool (optional, default: ``False``)
        Whether to also compute the size of a largest component and its link
        density as a function of time. This has no effect if ``temporal == False``.
        When ``True``, for large networks this will significantly increase the 
        computational time.
    temporal_resol : int (optional, default: ``1``)
        The resolution of the temporal structure. The temporal structure is saved
        only at each ``temporal_resol``'th step. 
    vertex_time_series : bool (optional, default: ``False``)
        Whether to save total amounts of resource on vertices at each time step. 
        The values are saved in ``vertex_time_series_file`` (See below) so that
        each line corresponds to a single time step. 
    temporal_summary_file : str (optional, default: ``temporal.csv``)
        The filename in which to save the temporal_summary. The file can be 
    vertex_time_series_file : str (optional, default: ``vertex_time_series.csv``)
        The file in which to save the ``vertex_time_series``. The file can be 
        created in any directory by passing an appropriate path.
    vertex_data : bool (optional, default: ``False``)
        Whether to save individual vertex data or not. This includes group 
        label of each vertex, its lifetime, and its starting and final degrees.
        The data storage is controlled by ``vertex_file`` (See below). 
    vertex_file : str (optional, default: ``vertex.csv``)
        The filename in which to save ``vertex_data``. The file can be created
        in any directory by passing an appropriate path.
    seed : int (optional, default: ``None``)
        The seed to be used for the random number generator. This must be an
        integer in the range 0 and (2**32-1). For a fixed seed, the function 
        will produce identical output each time it is called. ``seed=0`` and 
        ``seed=1`` produce identical outputs. 

    Returns
    -------
    num_vulnerable : int 
        Total number of vertices which became vulnerable over the lifetime of 
        the network. If a vertex becomes vulnerable multiple times, those
        occurrences are counted separately. 
    num_saved : int 
        Total number of vertices saved over the lifetime of the network. If a 
        vertex is saved more than once, those occurrences are counted 
        separately.
    vert_t : :class:`numpy.ndarray`
        Array containing lifetimes of vertices in the network.
    wastage : :class: `numpy.ndarray`
        Array containing the total wastage amount for each vertex

    Examples:
    ---------

.. testcode::
    
   >>> import graph_tool.all as gt
   >>> import dependency_networks as dn
   >>> g = gt.collection.data['karate']
   >>> n = g.num_vertices()
   >>> dn.network_activity(n, g, death=True, seed=42)
   (80, 46, array([14,  4,  6,  3,  1, 11,  1,  1,  7,  2, 11,  1,  1,  3, 20,
        1,  1, 1,  1,  2,  4,  5, 11, 10,  8,  9,  4,  6,  6, 11,  4,  6, 20,
        20], dtype=int64))

    References
    ----------
    .. [ingale-resource-2020] Madhusudan Ingale and Snehal M. Shekatkar, 
       "Resource dependency and survivability in complex networks". 
       Physical Review E 102, no. 1: 062304 (2020) 
       :doi:`10.1103/PhysRevE.102.062304`, :arXiv:`2006.07082`
    """

    # Pass distribution parameters to the parameter array 'beta'
    beta = np.empty((2, n))
    if prob_dist == 'uniform':
        beta[0, :] = dist_params["a"]
        beta[1, :] = dist_params["b"]
    elif prob_dist == 'exponential':
        beta[0, :] = dist_params["beta"]
    elif prob_dist == 'gaussian':
        beta[0, :] = dist_params["mu"]
        beta[1, :] = dist_params["sigma"]
    elif prob_dist == 'gaussian_positive':
        beta[0, :] = dist_params["mu"]
        beta[1, :] = dist_params["sigma"]
    elif prob_dist == 'pareto':
        beta[0, :] = dist_params["alpha"]
        beta[1, :] = dist_params["xm"]
    elif prob_dist in ('chi', 'chi_squared') :
        beta[0, :] = dist_params["dof"]
    elif prob_dist in ('logistic', 'logistic_positive'):
        beta[0, :] = dist_params["mu"]
        beta[1, :] = dist_params["a"]
    elif prob_dist == 'gamma':
        beta[0, :] = dist_params["a"]
        beta[1, :] = dist_params["beta"]
                
    # When R, eta, or ext_prop_vals are supplied as floats or ints, 
    # they must be converted to numpy arrays
    if isinstance(R, int) or isinstance(R, float):
        R = R * np.ones(n)
    if isinstance(eta, int) or isinstance(eta, float):
        eta = eta * np.ones(n)
    if isinstance(masking_verts_ext_prop_vals, int)  \
        or isinstance(masking_verts_ext_prop_vals, float):
        masking_verts_ext_prop_vals = masking_verts_ext_prop_vals * np.ones(n)
    if isinstance(masked_nbrs_ext_prop_vals, int)  \
        or isinstance(masked_nbrs_ext_prop_vals, float):
        masked_nbrs_ext_prop_vals = masked_nbrs_ext_prop_vals * np.ones(n)

    # Construct adjacency list(s) for the input graph
    try:
        import graph_tool as gt
        if isinstance(g, gt.Graph):
            from dependency_networks import auxiliary as ax
            if g.is_directed():
                directed = True
                adj_list_out, weights_adj_list_out, deg_out = \
                    ax._get_adj_list(g, deg='out', multigraph=multigraph, 
                        weighted=weighted)
                adj_list_in, weights_adj_list_in, deg_in = \
                    ax._get_adj_list(g, deg='in', multigraph=multigraph, 
                        weighted=weighted)
            else:
                directed = False
                adj_list_out, weights_adj_list_out, deg_out = \
                    ax._get_adj_list(g, deg="out", multigraph=multigraph, weighted=weighted)
                # Construct dummy arrays to pass to Fortran since the graph is undirected
                deg_in = deepcopy(deg_out)
                adj_list_in = deepcopy(adj_list_out)
                weights_adj_list_in = deepcopy(weights_adj_list_out)
    except ModuleNotFoundError:
        pass

    try:
        import networkx as nx
        if isinstance(g, nx.DiGraph):
            directed = True
            from dependency_networks import auxiliary as ax
            adj_list_in, weights_adj_list_in, deg_in = ax._get_adj_list(g, 
                deg="in", multigraph=multigraph)
            adj_list_out, weights_adj_list_out, deg_out = ax._get_adj_list(g, 
                deg="out", multigraph=multigraph)
        elif isinstance(g, nx.Graph):
            directed = False
            from dependency_networks import auxiliary as ax
            adj_list_out, weights_adj_list_out, deg_out = ax._get_adj_list(g, 
                multigraph=multigraph)
            # Construct dummy arrays to pass to Fortran since the graph is undirected
            deg_in = deepcopy(deg_out)
            adj_list_in = deepcopy(adj_list_out)
            weights_adj_list_in = deepcopy(weights_adj_list_out)
    except ModuleNotFoundError:
        pass

    if tot_groups <= 0:
        raise ValueError("The total number of groups must be at least 1")
    if tot_groups == 1:
        group_label = np.zeros(shape=n)
    if tot_groups > 1 and group_label is None:
        raise ValueError("When more than one groups are present, the group "
            "labels must be provided")

    # Process masking_verts_conds to pass to Fortran
    if prob_dist in ("exponential", "chi", "chi_squared"):
        masking_verts_array = np.zeros(shape=(2, 8), dtype='int')
        mask_verts_prop_vals = np.zeros(8)
    elif prob_dist in ("uniform", "gaussian", "gaussian_positive", "pareto",
        "logistic", "logistic_positive", "gamma"):
        masking_verts_array = np.zeros(shape=(2, 9), dtype='int')
        mask_verts_prop_vals = np.zeros(9)

    if all_verts_mask == False:
        if isinstance(masking_verts_conds, list) == False:
            raise TypeError("masking_verts_conds must be a list of tuples")
        if len(masking_verts_conds) > 0:
            from dependency_networks import auxiliary as ax
            for tup in masking_verts_conds:
                try : 
                    prop, cond, val = tup
                except ValueError:
                    raise ValueError(f"Each tuple must contain exactly three strings.")
                if ((not isinstance(prop, str)) or (not isinstance(cond, str) or 
                not isinstance(val, str))):
                    raise ValueError("Each element of the masking tuple must be "
                        "of type str")
                ix = ax._nbr_prop_to_index(prop)
                masking_verts_array[0][ix] = 1
                masking_verts_array[1][ix] = ax._condition_to_int(cond)
                mask_verts_prop_vals[ix] = float(val)

    # Process masked_nbrs_conds to pass to Fortran
    if not isinstance(masked_nbrs_conds, list):
        raise TypeError("masked_nbrs_conds must be a list of tuples")
    if prob_dist in ("exponential", "chi", "chi_squared"):
        masked_nbrs_array = np.zeros(shape=(2, 8))
        mask_nbrs_prop_vals = np.zeros(8)
    elif prob_dist in ("uniform", "gaussian", "gaussian_positive", "pareto", 
        "logistic", "logistic_positive", "gamma"):
        masked_nbrs_array = np.zeros(shape=(2, 9))
        mask_nbrs_prop_vals = np.zeros(9)
    if len(masked_nbrs_conds) > 0:
        from dependency_networks import auxiliary as ax
        for tup in masked_nbrs_conds:
            try : 
                prop, cond, val = tup
            except ValueError:
                raise ValueError(f"Each tuple must contain exactly three strings.")
            if ((not isinstance(prop, str)) or (not isinstance(cond, str) or 
            not isinstance(val, str))):
                raise ValueError("Each element of the masking tuple must be "
                    "of type str")
            ix = ax._nbr_prop_to_index(prop)
            masked_nbrs_array[0][ix] = 1
            masked_nbrs_array[1][ix] = ax._condition_to_int(cond)
            mask_nbrs_prop_vals[ix] = float(val)

    # Seed for the random number generator
    use_seed = False
    if seed is None:
        seed = 0 # A random seed will be generated inside fortran
    else:
        use_seed = True

    tot_groups = np.unique(group_label).size
    # Fortran/C interface
    num_vulnerable, num_saved, vert_t, wastage = \
        rd.resource_dependency.vertex_dependency(n, death, tot_iter, temporal, 
        temporal_lc, vertex_data, temporal_resol, vertex_time_series, beta, R,
        eta, prob_dist, tot_groups, directed, weighted, deg_out, deg_in, 
        adj_list_out, adj_list_in, weights_adj_list_out, weights_adj_list_in, 
        group_label, all_verts_mask, masking_verts_array, mask_verts_prop_vals, 
        masking_verts_ext_prop_vals, masking_verts_OR, masked_nbrs_array, 
        mask_nbrs_prop_vals, masked_nbrs_ext_prop_vals, masked_nbrs_OR, 
        temporal_summary_file, vertex_file, vertex_time_series_file, seed, 
        use_seed)
    return int(num_vulnerable), int(num_saved), vert_t, wastage
