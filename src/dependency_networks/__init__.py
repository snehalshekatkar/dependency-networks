#    This file is part of the package dependency-networks
#
#    Copyright (C) 2020 Snehal Madhukar Shekatkar <snehal@inferred.in>
#
#    This package is free software: you can redistribute it and/or modify
#    it under the terms of the GNU General Public License as published by
#    the Free Software Foundation, either version 3 of the License, or
#    (at your option) any later version.
#
#    The package is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
#    GNU General Public License for more details.
#
#    You should have received a copy of the GNU General Public License
#    along with this package. If not, see <https://www.gnu.org/licenses/>.

"""
dependecy-networks
==================

An efficient python package to study dependency networks

The docstring examples assume that `dependency_networks` has been imported as `dn`::

    >>> from graph_tool import collection
    >>> g = collection.data['karate']
    >>> n = g.num_vertices()
    >>> tot_vuln, tot_saved, vert_t, wastage = dn.network_activity(n, g, death=True, seed=0)
    >>> vert_t
     array([20, 20,  9, 20,  2,  2,  7,  8,  4,  1,  2,  1, 11,  4,  2,  4,  2,
            1,  2,  4,  2,  6,  6, 14,  3,  2,  3,  9,  4,  3,  3,  4, 14,  7],
           dtype=int64))
"""
__version__ = '5.0.0'
__author__ = 'Snehal M. Shekatkar <snehal@inferred.in>'
__copyright__ =  'Copyright 2020 Snehal M. Shekatkar'
__license__ = 'GPL v3 or above'

from .resdep import network_activity
from .reqoff import request_offer_model
from .auxiliary import _get_adj_list
