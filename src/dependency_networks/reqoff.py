#    This file is part of the package dependency-networks
#
#    Copyright (C) 2020 Snehal Madhukar Shekatkar <snehal@inferred.in>
#
#    This package is free software: you can redistribute it and/or modify
#    it under the terms of the GNU General Public License as published by
#    the Free Software Foundation, either version 3 of the License, or
#    (at your option) any later version.
#
#    The package is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
#    GNU General Public License for more details.
#
#    You should have received a copy of the GNU General Public License
#    along with this package. If not, see <https://www.gnu.org/licenses/>.

"""
Simulate the request-offer model described in :
    .. [agrawal-effect-2022] Harshit Agrawal and Ashwin Lahorkar and 
       Snehal M. Shekatkar, "Effect of money heterogeneity on resource 
       dependency in complex networks". :arxiv:`2206.06666`
"""

import os
import numpy as np
from copy import deepcopy
#import resource_dependency as rd
import request_offer as ro

def request_offer_model(n, g, multigraph=False, weighted=False, 
    death=False, prob_dist="exponential", dist_params={"beta" : 2.0}, 
    money=1.0, unit_resource_price=1.0, R=1.0, eta=0.0, tot_iter=100,
    strategy='random', temporal=False, temporal_lc=False,
    temporal_resol=1, vertex_data=False, money_data=False, 
    reqoff_count_t=-1, vertex_time_series=False, 
    temporal_summary_file='temporal.csv', vertex_file='vertex.csv', 
    money_file='money.csv', vertex_time_series_file='vert_ts.csv', 
    seed=None):
    r"""
    Simulate the request offer model of resource dependency.

    Parameters
    ----------
    n : int
        number of vertices in the original graph.
    g : :class:`graph_tool.Graph`
        The network on which to simulate the model.
    multigraph : bool (optional, default: ``False``)
        Whether to consider the graph as a multi-graph or not. If 
        ``False``, all multi-edges between distinct vertices are 
        collapsed to a single edge, while all self-loops are removed. 
        If the graph is a networkx graph, it must have a correct type 
        (nx.MultiGraph or nx.MultiDiGraph).
    weighted : bool (optional, default: ``False``)
        Whether to take into account edge weights which must be 
        provided in the form of an internal property map for a 
        graph-tool graph or in the form of an edge attribute for a 
        networkx graph.
    death : bool (optional, default: ``False``)
        Whether a vertex dies when total resouce on it becomes less 
        than its thershold.
    prob_dist : str (optional, default: ``"exponential"``)
        The probability distribution to be used to generate resource at each 
        vertex. Available distributions are:

        ``uniform``
            The uniform distribution over :math:`(a, b)`.
        ``exponential``
            The Exponential distribution :math:`Exp(\beta)` with probability density: 

            .. math::
                p(x, \beta) = \frac{1}{\beta}e^{-\frac{x}{\beta}}
        ``gaussian``
            The Gaussian/Normal distribution :math:`\mathcal{N}(\mu, \sigma^2)` with
            mean :math:`\mu` and standard deviation :math:`sigma`.
        ``gaussian_positive``
            Modified Gaussian/Normal distribution obtained by discarding the negative 
            tail of the usual Gaussian distribution :math:`\mathcal{N}(\mu, \sigma^2)` 
            with mean :math:`\mu` and standard deviation :math:`sigma`.
        ``pareto``
            The Pareto distribution with probability density:

            .. math::
                p(x, \alpha, x_m) = \frac{\alpha x_m^\alpha}{x^{\alpha+1}}\quad \text{for}\ x \ge x_m
        ``chi``
            The Chi distribution with `dof` degrees of freedom.
        ``chi-squared``
            The Chi-squared distribution with `dof` degrees of freedom.
        ``logistic``
            The Logistic distribution with location parameter :math:`\mu` and scale parameter :math:`a`.
        ``logistic_positive``
            Modified Logistic distribution obtained by discarding the negative tail
            of the usual Logistic distribution with location parameter :math:`\mu` and 
            scale parameter :math:`a`.
        ``gamma``
            The Gamma distribution with shape parameter `a` and scale parameter :math:`beta`.
            
    dist_params : dict (optional, default: ``{"beta" : 2.0}``)
        Parameters of the probability distribution ``prob_dist`` being used. 
        An array of length ``n`` can be passed for each parameter so as to
        have different values for different vertices. The parameters for 
        different distributions are:

        ``uniform``           : ``a``, ``b``
        ``exponential``       : ``beta``
        ``gaussian``          : ``mu``, ``sigma``
        ``gaussian_positive`` : ``mu``, ``sigma``
        ``pareto``            : ``alpha``, ``xm``
        ``chi``               : ``dof``
        ``chi-squared``       : ``dof``
        ``logistic``          : ``mu``, ``a``
        ``logistic_positive`` : ``mu``, ``a``
        ``gamma``             : ``a``, ``beta``

    money : float or :class:`numpy.ndarray` (optional, default: ``1.0``)
        The amount of money each vertex has initially. 
    unit_resource_price : float (optional, default: ``1.0``)
        The amount of money that must be paid to buy a unit amount of 
        resource.
    R : float or :class:`numpy.ndarray` (optional, default: ``1.0``)        
        The survival threshold for a vertex. If the total amount of resource
        is less than R, the vertex dies immediately. 
    eta : float or :class:`numpy.ndarray` (optional, default: ``0.0``)
        Distribution bias. The part of the surplus amount that the neighbour 
        :math:`j` receives is proportional to :math:`1/k_j^{\eta}`. For 
        directed networks, :math:`k_j` denotes the in-degree of :math:`j`.
    tot_iter : int (optional, default: ``100``)
        The total number of iterations to perform. This has no effect 
        when ``death == True`` since in that case iterations are 
        performed until the whole network dies.
    strategy : str (optional, default: ``random``)
        Could be one of ``random``, ``low_to_high``, ``high_to_low``, 
        ``prop_to_req``, ``prop_to_req_deg``.
    temporal : bool (optional, default: ``False``)
        Whether to save the summary of the temporal structure of the 
        network. The summary includes the size of the network, the 
        average degree of the network, average amount of money each 
        alive vertex has, the sample standard deviation of the money on
        the vertices that are alive, and maximum and minimum values of 
        the money among the alive vertices. If ``temporal_lc==True`` 
        (see below), the summary also includes the size of the largest 
        component and its density (till its size is at least 2). The 
        output is stored in `temporal_summary_file' (see below). 
    temporal_lc : bool (optional, default: ``False``)
        Whether to also compute the size of a largest component and its link
        density as a function of time. This has no effect if temporal==False.
        When ``True``, for large networks this will significantly increase the 
        computational time.
    temporal_resol : int (optional, default: 1)
        The resolution of the temporal structure. The temporal structure is saved
        only at each `temporal_resol`'th step. 
    vertex_data : bool (optional, default: ``False``)
        Whether to save individual vertex data or not. Setting it 
        ``True`` has effect only if ``temporal==True``. The data 
        storage is controlled by vertex_file. The data includes vertex 
        index, start and end degrees, total time for which the vertex 
        survived, initial and final money on it, total requests it sent
        and total offers it received over its lifetime, and total 
        resource wasted by it over its lifetime.
    money_data : bool (optional, default: ``False``)
        Whether to store money on each vertex for each time step. Setting it 
        ``True`` has effet only if temporal==``True``. The data storage is 
        controlled by money_file. Give a bare filename without any path. The 
        file is created in the current directory.
    reqoff_count_t : int (optional, default: -1)
        The time value up to which to count total number of requests sent and 
        offers received by the vertices. By default, the counting is done until
        all the vertices die. This has effect only if vertex_data == ``True``.
    vertex_time_series : bool (optional, default: ``False``)
        Whether to save activity states of vertices at each time step. 
        The states are saved in ``vertex_time_series_file`` (See below) so that
        each line corresponds to a single time step. In this file, ``0`` 
        indicates an inactive state while ``1`` indicates an active state.
        If ``death == True``, this file instead contains base_resource values 
        of the vertices at each time step. 
    temporal_summary_file : str (optional, default: ``temporal.csv``)
        The filename in which to save the temporal_summary. The file can be 
        created in any directory by passing an appropriate path.
    vertex_file : str (optional, default: ``vertex.csv``)
        The filename in which to save ``vertex_data``. The file can be created
        in any directory by passing an appropriate path.
    money_file : str (optional, default: ``money.csv``)
        The filename in which to save the money on individual vertices at each 
        time step. The file can be created in any directory by passing
        an appropriate path.
    vertex_time_series_file : str (optional, default: ``vertex_time_series.csv``)
        The file in which to save the ``vertex_time_series``. The file can be 
        created in any directory by passing an appropriate path.
    seed : int (optional, default: ``None``)
        The seed to be used for the random number generator. This must be an
        integer in the range 0 and (2**32-1). For a fixed seed, the function 
        will produce identical output each time it is called.

    Returns
    -------
    verts_count : tuple
        A tuple containing the total number of vulnerable vertices, the
        total number of vertices saved out of the vulnerable ones, and 
        the total number of vertices died because of the lack of money
        over the lifetime of the network. If a vertex becomes vulnerable
        multiple times or gets saved multiple times, those occurrences
        are counted separately.
    wastage : float
        Total amount of resource wasted per unit time per vertex.
    vert_t : :class:`numpy.ndarray`
        Array containing lifetimes of vertices in the network.

    Examples:
    ---------

.. testcode::

   >>> import graph_tool.all as gt
   >>> import dependency_networks as dn
   >>> g = gt.collection.data['karate']
   >>> n = g.num_vertices()
   >>> dn.request_offer_model(n, g, death=True, seed=42)
   ((74, 40, 13), 0.8312838429938975, array([ 8, 13,  2, 11,  1,  2,  1,  2, 
        11,  6,  5,  1,  2,  3,  5,  1,  1, 4,  5,  4,  9,  3,  4,  6,  5,  2,
        5,  7,  3,  2, 14, 10, 11, 10], dtype=int64))

    References
    ----------
    .. [agrawal-effect-2022] Harshit Agrawal and Ashwin Lahorkar and 
       Snehal M. Shekatkar, "Effect of money heterogeneity on resource 
       dependency in complex networks". :arxiv:`2206.06666`
    """

    # Pass distribution parameters to the parameter array 'beta'
    beta = np.empty((2, n))
    if prob_dist == 'uniform':
        beta[0, :] = dist_params["a"]
        beta[1, :] = dist_params["b"]
    elif prob_dist == 'exponential':
        beta[0, :] = dist_params["beta"]
    elif prob_dist == 'gaussian':
        beta[0, :] = dist_params["mu"]
        beta[1, :] = dist_params["sigma"]
    elif prob_dist == 'gaussian_positive':
        beta[0, :] = dist_params["mu"]
        beta[1, :] = dist_params["sigma"]
    elif prob_dist == 'pareto':
        beta[0, :] = dist_params["alpha"]
        beta[1, :] = dist_params["xm"]
    elif prob_dist in ('chi', 'chi_squared') :
        beta[0, :] = dist_params["dof"]
    elif prob_dist in ('logistic', 'logistic_positive'):
        beta[0, :] = dist_params["mu"]
        beta[1, :] = dist_params["a"]
    elif prob_dist == 'gamma':
        beta[0, :] = dist_params["a"]
        beta[1, :] = dist_params["beta"]
    # When R, eta, or money are supplied as floats or ints, 
    # they must be converted to numpy arrays
    if isinstance(R, int) or isinstance(R, float):
        R = R * np.ones(n)
    if isinstance(eta, int) or isinstance(eta, float):
        eta = eta * np.ones(n)
    if isinstance(money, int) or isinstance(money, float):
        money = money * np.ones(n)

    # If the supplied graph is an instance of the class gt.Graph or nx.Graph, 
    # adjacency list(s) need(s) to be constructed
    try:
        import graph_tool as gt
        if isinstance(g, gt.Graph):
            from dependency_networks import auxiliary as ax
            if g.is_directed():
                directed = True
                adj_list_out, weights_adj_list_out, deg_out = \
                    ax._get_adj_list(g, deg='out', multigraph=multigraph, 
                    weighted=weighted)
                adj_list_in, weights_adj_list_in, deg_in = \
                    ax._get_adj_list(g, deg='in', multigraph=multigraph, 
                    weighted=weighted)
            else:
                directed = False
                adj_list_out, weights_adj_list_out, deg_out = \
                    ax._get_adj_list(g, multigraph=multigraph, weighted=weighted)
                # Construct dummy arrays to pass to Fortran since the graph is undirected
                deg_in = deepcopy(deg_out)
                adj_list_in = deepcopy(adj_list_out)
                weights_adj_list_in = deepcopy(weights_adj_list_out)
    except ModuleNotFoundError:
        pass

    try:
        import networkx as nx
        if isinstance(g, nx.DiGraph) or isinstance(g, nx.MultiDiGraph):
            directed = True
            from dependency_networks import auxiliary as ax
            adj_list_in, weights_adj_list_in, deg_in = ax._get_adj_list(g, 
                deg="in", weighted=weighted, multigraph=multigraph)
            adj_list_out, weights_adj_list_out, deg_out = ax._get_adj_list(g, 
                deg="out", weighted=weighted, multigraph=multigraph)
        elif isinstance(g, nx.Graph) or isinstance(g, nx.MultiGraph):
            directed = False
            from dependency_networks import auxiliary as ax
            adj_list_out, weights_adj_list_out, deg_out = ax._get_adj_list(g, 
                weighted=weighted, multigraph=multigraph)
            # Construct dummy arrays to pass to Fortran since the graph is undirected
            deg_in = deepcopy(deg_out)
            adj_list_in = deepcopy(adj_list_out)
            weights_adj_list_in = deepcopy(weights_adj_list_out)
    except ModuleNotFoundError:
        pass

    # Seed for the random number generator
    use_seed = False
    if seed is None:
        seed = 0 # A random seed will be generated inside fortran
    else:
        use_seed = True

    tot_vulnerable, tot_saved, money_deficient_verts, wastage, vert_t = \
        ro.request_offer.request_offer_model(n, death, tot_iter, temporal, 
        temporal_lc, temporal_resol, vertex_time_series, deg_out, deg_in, 
        adj_list_out, adj_list_in, multigraph, weighted, weights_adj_list_out,
        weights_adj_list_in, beta, R, eta, prob_dist, money, 
        unit_resource_price, strategy, temporal_summary_file, vertex_data,
        vertex_file, money_data, money_file, reqoff_count_t, seed, use_seed, 
        vertex_time_series_file)
    verts_count = (tot_vulnerable, tot_saved, money_deficient_verts)
    return verts_count, wastage, vert_t 

