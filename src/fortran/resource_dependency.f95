!    This file is part of the package dependency-networks
!
!    Copyright (C) 2020-2021 Snehal Madhukar Shekatkar <snehal@inferred.in>
!
!    This package is free software: you can redistribute it and/or modify
!    it under the terms of the GNU General Public License as published by
!    the Free Software Foundation, either version 3 of the License, or
!    (at your option) any later version.
!
!    The package is distributed in the hope that it will be useful,
!    but WITHOUT ANY WARRANTY; without even the implied warranty of
!    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
!    GNU General Public License for more details.
!
!    You should have received a copy of the GNU General Public License
!    along with this program. If not, see <https://www.gnu.org/licenses/>.

module resource_dependency 
    use random
    use topology
    use auxiliary
    use delete_vertex

    implicit none

    contains

    subroutine vertex_dependency(n, death, tot_iter, temporal, temporal_lc,   & 
        & vertex_data, temporal_resol, vertex_time_series, beta, RR, eta,     & 
        & prob_dist, tot_groups, directed, weighted, deg_out, deg_in,         &
        & adj_list_out, adj_list_in, weights_adj_list_out,                    &
        & weights_adj_list_in, group_label, all_verts_mask,                   &
        & masking_verts_array, mask_verts_prop_vals,                          &
        & masking_verts_ext_prop_vals, masking_verts_OR, masked_nbrs_array,   &
        & mask_nbrs_prop_vals, masked_nbrs_ext_prop_vals, masked_nbrs_OR,     &
        & temp_summary_file, vertex_file, vertex_time_series_file, seed,      &
        & use_seed, tot_vulnerable, tot_saved, vert_t, wastage)

        implicit none
        integer, parameter :: int10 = selected_int_kind(10), real10 = selected_real_kind(10,10)
        character(len=100) :: fmt, temp_summary_file, vertex_file, prob_dist, vertex_time_series_file
        logical :: temporal, temporal_lc, vertex_data, directed, weighted, masking_verts_OR, masked_nbrs_OR, use_seed
        logical :: death, mask_vert, mask_nbr, all_verts_mask, vertex_time_series, unsafe(n)
        logical :: death_condition, activity_condition
        integer (kind=int10) :: i, j, k, u, v, t, n, m1, m2, ix, nbr, max_deg, tot_vert, seed
        integer (kind=int10) :: tot_deleted, tot_nbrs, counter, largest_comp_edges, temporal_resol
        integer (kind=int10) :: tot_groups, tot_comps, largest_comp_index, largest_comp_size
        integer (kind=int10) :: tot_vulnerable, tot_saved, tot_params, vertex_active(n)
        integer (kind=int10) :: masking_verts_array(:, :), masked_nbrs_array(:, :)

        real (kind=real10) :: mask_verts_prop_vals(:), mask_nbrs_prop_vals(:)
        real (kind=real10) :: deg_second_moment, ave_surplus
        real (kind=real10) :: ave_deg_out, ave_deg_in, ave_deg_largest_comp, tot_resource, ave_vert_time
        real (kind=real10) :: ave_surplus_analytical, largest_comp_link_density
        real (kind=real10) :: r, beta(:, :), RR(:), eta(:), inv_deg_sum, C
        real (kind=real10) :: masking_verts_ext_prop_vals(:), masked_nbrs_ext_prop_vals(:)
        real (kind=real10) :: numerator, denominator, mu, sigma

        integer (kind=int10) :: tot_iter
        integer (kind=int10) :: deg_in(:), deg_out(:), adj_list_in(:, :), adj_list_out(:, :), group_label(:)
        integer (kind=int10) :: deg_in_temp(n), deg_out_temp(n), group_size(tot_groups)
        integer (kind=int10) :: not_deleted(n), vert_list(n), new_vert_list(n)
        integer (kind=int10) :: component(n), comp_vert(n), comp_edges(n), vert_t(n)
        real (real10) :: deg_out_weights(n), deg_out_temp_weights(n), deg_in_weights(n), deg_in_temp_weights(n)
        real (kind=real10) :: base_resource(n), temp_resource(n), weights_adj_list_out(:, :), weights_adj_list_in(:, :)
        real (kind=real10) :: group_ave_deg_in(tot_groups), group_ave_deg_out(tot_groups), wastage(n)

!f2py   intent(in) :: n, beta, RR, eta, tot_groups, masked_nbrs_array, masked_nbrs_ext_prop_vals
!f2py   intent(in) :: directed, deg_in, deg_out, adj_list_in, adj_list_out, seed, use_seed
!f2py   intent(in) :: weights_adj_list_out, weights_adj_list_in, prob_dist, all_verts_mask, death, vertex_time_series
!f2py   intent(in) :: masking_verts_OR, masked_nbrs_OR, masking_verts_array, masking_verts_ext_prop_vals
!f2py   intent(in) :: group_label, temporal, temporal_lc, vertex_data, temporal_resol
!f2py   intent(in) :: temp_summary_file, vertex_file, vertex_time_series_file, tot_iter
!f2py   intent(out) :: tot_vulnerable, tot_saved, vert_t, wastage

        IF (use_seed .eqv. .False.) THEN
            ! Generate a random integer between 2 and 100000000 
            ! to be used as a seed for GSL functions
            m1 = 2; m2 = 100000000
            call random_number(r)
            seed = m1 + FLOOR(r*(m2-m1+1))
        ENDIF
        ! Initialize the random number generator and set seed for it
        call initialize_rng(seed)
        IF (temporal .eqv. .True.) THEN
            open(unit=1, file=temp_summary_file)
            If (temporal_lc .eqv. .True. .and. directed .eqv. .False.) Then
                write(1, '(A)')"time,network_size,largest_comp_size,largest_comp_link_density,ave_deg"
            Else
                write(1, '(A)')"time,network_size,ave_deg"
            Endif
            If (tot_groups > 1) Then
                open(unit=3, file="./group_sizes.csv")
                if (directed .eqv. .False.) then
                    open(unit=4, file="./group_ave_degs.csv")
                else
                    open(unit=4, file="./group_ave_degs_out.csv")
                    open(unit=5, file="./group_ave_degs_in.csv")
                endif
            Endif
        ENDIF

        IF (vertex_data .eqv. .True.) THEN
            open(unit=2, file=vertex_file)
            If (death .eqv. .True.) THEN
                write(2, '(A)')"vert_index,group_label,start_out_deg,end_out_deg,start_in_deg,end_in_deg,time"
            Else
                write(2, '(A)')"vert_index,group_label,start_out_deg,start_in_deg"
            Endif
        ENDIF

        vertex_active = 1
        ! Change group labels to Fortran convention
        group_label = group_label + 1

        ! Vertex time series file
        IF (vertex_time_series .eqv. .True.) THEN
            open(unit=7, file=vertex_time_series_file)           
        ENDIF


        ! Initializations
        vert_t = 0
        deg_in_temp = deg_in
        deg_out_temp = deg_out
        deg_out_temp_weights = 0
        deg_in_temp_weights = 0
        wastage = 0
        group_size = 0
        not_deleted = 1 ! No vertex is in deleted state initially
        tot_vert = n
        DO i = 1, n
            ! This stores the list of vertices that are not deleted.
            ! Although this is always an array of length n, only first 
            ! tot_vert of these are alive at any time since dead ones are
            ! pushed to the end of the array. 
            vert_list(i) = i
            ! Initialize the group sizes
            group_size(group_label(i)) = group_size(group_label(i)) + 1
        ENDDO
        ! If the graph is weighted, save the sum of the edge weights around each vertex
        IF (weighted .eqv. .True.) then
            DO i = 1, n
                v = vert_list(i)
                deg_out_weights(v) = sum(weights_adj_list_out(1:deg_out(v), v))
                deg_in_weights(v) = sum(weights_adj_list_in(1:deg_in(v), v))
                deg_out_temp_weights(v) = deg_out_weights(v)
                deg_in_temp_weights(v) = deg_in_weights(v)
            ENDDO
        ENdIF
        t = 0
        tot_vulnerable = 0
        tot_saved = 0
        ave_vert_time = 0
        ! Define death and activity conditions to decide whether vertices die or not
        death_condition    = (death .eqv. .True.) .and. (tot_vert > 0)
        activity_condition = (death .eqv. .False.) .and. (t < tot_iter)

        ! Run the time loop
        DO while (death_condition .or. activity_condition)
            ! Update death and activity conditions
            death_condition    = (death .eqv. .True.) .and. (tot_vert > 0)
            activity_condition = (death .eqv. .False.) .and. (t < tot_iter)

            max_deg = maxval(deg_out)

            !====== Temporal 'IF-ENDIF' block starts ========================
            IF ((temporal .eqv. .True.) .and. (mod(t, temporal_resol) == 0)) THEN
                ! Find the largest component
                If (temporal_lc .eqv. .True. .and. directed .eqv. .False.) Then
                    call BFS(n, max_deg, adj_list_out, deg_out_temp, component, & 
                        & comp_vert, comp_edges)
                    ! Get the number of vertices and edges in the largest component
                    ix = maxloc(comp_vert, dim = 1) ! Get the index of a vertex 
                                                    ! that belongs to the largest 
                                                    ! component
                    largest_comp_size = comp_vert(ix)
                    largest_comp_edges = comp_edges(ix)
                    ! Calculate the link density of the largest component
                    numerator = 2. * real(largest_comp_edges, 8) 
                    denominator = largest_comp_size * (largest_comp_size-1)
                    if (denominator > 0) then
                        largest_comp_link_density = numerator / denominator
                    endif
                Endif

                ! Calculate the average degree
                If (sum(deg_out_temp) > 0) Then
                    ave_deg_out = real(sum(deg_out_temp), 8) / tot_vert
                Else
                    ave_deg_out = 0 
                Endif

                If (temporal_lc .eqv. .True. .and. directed .eqv. .False.) Then
                    write(1, '(3(I0,A),F0.3,A,F0.3)') t, ",", tot_vert, ",", largest_comp_size, ",", & 
                        & largest_comp_link_density, ",", ave_deg_out
                Else
                    write(1, '(2(I0,A),F0.3)') t, ",", tot_vert, ",", ave_deg_out
                Endif

                ! Calculate the average degrees of groups
                If (tot_groups > 1) Then
                    group_ave_deg_in = 0
                    group_ave_deg_out = 0
                    Do i = 1, tot_vert
                        group_ave_deg_in(group_label(vert_list(i))) = & 
                           & group_ave_deg_in(group_label(vert_list(i))) + deg_in_temp(vert_list(i))
                        group_ave_deg_out(group_label(vert_list(i))) = & 
                           & group_ave_deg_out(group_label(vert_list(i))) + deg_out_temp(vert_list(i))
                    Enddo
                    Do i = 1, tot_groups
                        if (group_size(i) > 0) then
                            group_ave_deg_out(i) = group_ave_deg_out(i) / group_size(i)
                            if (directed .eqv. .True.) then
                                group_ave_deg_in(i) = group_ave_deg_in(i) / group_size(i)
                            endif
                        endif
                    Enddo
                    write(fmt, '(A,I0,A)')"(", tot_groups-1, "(F0.3,','),F0.3)"
                    write(4, fmt)group_ave_deg_out
                    if (directed .eqv. .True.) then
                        write(5, fmt)group_ave_deg_in
                    endif
                Endif

                ! Record the group sizes
                write(fmt, '(A,I0,A)')"(", tot_groups-1, "(I0, ','), I0)"
                If (tot_groups > 1) Then
                    write(3, fmt)group_size
                Endif
            ENDIF 
            !====== Temporal 'IF-ENDIF' block over ========================

            ! Generate resources at each vertex
            unsafe = .False.
            tot_resource = 0 ! This saves the total amount produced in 
                             ! the network at each time
            base_resource = 0
            Do i = 1, tot_vert
                v = vert_list(i)
                IF (prob_dist == 'uniform') THEN
                    tot_params = 2
                    call uniform(base_resource(v), beta(1, v), beta(2, v))
                ELSEIF (prob_dist == 'exponential') THEN
                    tot_params = 1
                    call exponential(base_resource(v), beta(1, v))
                ELSEIF (prob_dist == 'gaussian') THEN
                    tot_params = 2
                    call gaussian(base_resource(v), beta(1, v), beta(2, v))
                ELSEIF (prob_dist == 'gaussian_positive') THEN
                    tot_params = 2
                    call gaussian_positive(base_resource(v), beta(1,v), beta(2,v))
                ELSEIF (prob_dist == 'pareto') THEN
                    tot_params = 2
                    call pareto(base_resource(v), beta(1, v), beta(2, v))
                ELSEIF (prob_dist == 'chi') THEN
                    tot_params = 1
                    call chi(base_resource(v), beta(1, v))
                ELSEIF (prob_dist == 'chi_squared') THEN
                    tot_params = 1
                    call chi_squared(base_resource(v), beta(1, v))
                ELSEIF (prob_dist == 'logistic') THEN
                    tot_params = 2
                    call logistic(base_resource(v), beta(1, v), beta(2, v))
                ELSEIF (prob_dist == 'logistic_positive') THEN
                    tot_params = 2
                    call logistic_positive(base_resource(v), beta(1, v), beta(2, v))
                ELSEIF (prob_dist == 'gamma') THEN
                    tot_params = 2
                    call gamma_dist(base_resource(v), beta(1, v), beta(2, v))
                ENDIF
                ! Update the total amount produced in the network
                tot_resource = tot_resource + base_resource(v)
                IF (base_resource(v) < RR(v)) Then
                    unsafe(v) = .True.
                    tot_vulnerable = tot_vulnerable + 1
                ENDIF
            Enddo

            ! Distribute the surplus among the neighbours
            temp_resource = 0
            Do i = 1, tot_vert
                v = vert_list(i)
                IF (base_resource(v) > RR(v) .and. deg_out_temp(v) > 0) THEN
                    If (all_verts_mask .eqv. .False.) Then
                        call get_mask_vert(n, v, tot_params, base_resource(v), RR(v), beta(1:tot_params, v), &
                            & eta(v), deg_in(v), deg_in_temp(v), deg_out(v), deg_out_temp(v), &
                            & masking_verts_ext_prop_vals(v), masking_verts_array, mask_verts_prop_vals, &
                            & masking_verts_OR, mask_vert)
                    Else
                        mask_vert = .True.
                    Endif
                    ! Calculate the sharing constant
                    inv_deg_sum = 0     
                    sharing_const: do j = 1, deg_out_temp(v)
                        nbr = adj_list_out(j, v)
                        If (mask_vert .eqv. .True.) Then
                            ! Decide whether to mask the neighbour or not
                            call get_mask_vert(n, nbr, tot_params, base_resource(nbr), RR(nbr), &
                                & beta(1:tot_params, nbr), eta(nbr), deg_in(nbr), deg_in_temp(nbr), &
                                & deg_out(nbr), deg_out_temp(nbr), masked_nbrs_ext_prop_vals(nbr), &
                                & masked_nbrs_array, mask_nbrs_prop_vals, masked_nbrs_OR, mask_nbr)
                            if (mask_nbr .eqv. .False.) then
                                inv_deg_sum = inv_deg_sum + weights_adj_list_out(j,v)/deg_in_temp(nbr)**eta(nbr)
                            endif
                        Elseif (mask_vert .eqv. .False.) Then
                                inv_deg_sum = inv_deg_sum + weights_adj_list_out(j,v)/deg_in_temp(nbr)**eta(nbr)
                        Endif
                    enddo sharing_const
                    If (inv_deg_sum > 0.0) Then
                        C = (base_resource(v)-RR(v)) / inv_deg_sum
                    Else
                        C = 0
                    Endif
                    ! Loop over (out) neighbours of v
                    distribute_surplus: do j = 1, deg_out_temp(v)                
                        nbr = adj_list_out(j, v)
                        If (mask_vert .eqv. .True.) Then
                            call get_mask_vert(n, nbr, tot_params, base_resource(nbr), RR(nbr), beta(1:tot_params, nbr), &
                                & eta(nbr), deg_in(nbr), deg_in_temp(nbr), deg_out(nbr), deg_out_temp(nbr), &
                                & masked_nbrs_ext_prop_vals(nbr), masked_nbrs_array, mask_nbrs_prop_vals, masked_nbrs_OR, mask_nbr)
                            if (mask_nbr .eqv. .False.) then
                                temp_resource(nbr) = temp_resource(nbr) &
                                & + C * weights_adj_list_out(j,v) / deg_in_temp_weights(nbr)**eta(nbr)
                            endif
                        Elseif (mask_vert .eqv. .False.) Then
                            temp_resource(nbr) = temp_resource(nbr) &
                            & + C * weights_adj_list_out(j,v) / deg_in_temp_weights(nbr)**eta(nbr)
                        Endif
                    enddo distribute_surplus
                ENDIF
            Enddo
            Do i = 1, tot_vert
                v = vert_list(i)
                IF (base_resource(v) > RR(v) .and. deg_out_temp(v) > 0) THEN
                    base_resource(v) = RR(v)
                ENDIF
            Enddo            
            ! Update the base resources
            ave_surplus = 0
            Do i = 1, tot_vert
                v = vert_list(i)
                base_resource(v) = base_resource(v) + temp_resource(v)
                ave_surplus = ave_surplus + temp_resource(v)
            Enddo
    
            IF (death .eqv. .True.) THEN
                tot_deleted = 0
                ! Delete the vertices with less than RR base resource
                delete_loop:Do i = 1, tot_vert
                    v = vert_list(i)
                    ! Delete the vertex if it doesn't have enough resource
                    If (base_resource(v) < RR(v)) Then
                        ! If the graph is weighted, compute the total temp degree weights for vertex v
                        If (weighted .eqv. .True.) Then
                            deg_out_temp_weights(v) = 0
                            deg_in_temp_weights(v)  = 0
                            do j = 1, deg_out_temp(v)
                                deg_out_temp_weights(v) = deg_out_temp_weights(v) + weights_adj_list_out(j, v)
                            enddo
                            do j = 1, deg_in_temp(v)
                                deg_in_temp_weights(v)  = deg_in_temp_weights(v)  + weights_adj_list_in(j, v)
                            enddo
                        Endif
                        vertex_active(v) = 0
                        not_deleted(v) = 0
                        tot_deleted = tot_deleted + 1
                        If (vertex_data .eqv. .True.) Then
                            ! note down data of the deleted vertex 
                            if (weighted .eqv. .False.) then
                                write(2, '(6(I0,A),I0)')v-1, ",", group_label(v), ",", deg_out(v), ",", &
                                    & deg_out_temp(v), ",", deg_in(v), ",", deg_in_temp(v), ",", t+1
                            else
                                write(2, '(2(I0,A),4(F0.3,A),I0)')v-1, ",", group_label(v), ",", deg_out_weights(v), ",", &
                                    & deg_out_temp_weights(v), ",", deg_in_weights(v), ",", deg_in_temp_weights(v), ",", t+1
                            endif
                        Endif
                        ! update the size of the group to which the deleted vertex belongs
                        group_size(group_label(v)) = group_size(group_label(v)) - 1
                        ave_vert_time = ave_vert_time + t+1
                        vert_t(v) = t+1

                        ! Update the adjacency list(s)
                        call update_adjacency_lists(v, directed, deg_out, deg_out_temp, adj_list_out, & 
                            & weights_adj_list_out, weights_adj_list_in, deg_in, deg_in_temp, adj_list_in)

                    ! Otherwise update the number of saved vertices so far and the wastage
                    Elseif (base_resource(v) >= RR(v)) THEN
                        wastage(v) = wastage(v) + (base_resource(v)-RR(v))
                        if (unsafe(v) .eqv. .True.) then
                            tot_saved = tot_saved + 1
                        endif
                    Endif
                Enddo delete_loop
                ! Update the vertex list
                j = 1
                Do i = 1, tot_vert
                    ! Copy all the non-deleted vertices into new_vert_list
                    If (not_deleted(vert_list(i)) > 0) Then
                        new_vert_list(j) = vert_list(i)
                        j = j + 1
                    Endif
                Enddo
                tot_vert = tot_vert - tot_deleted
                ! Copy vertices from new_vert_list to vert_list
                If (tot_vert > 0) Then
                    vert_list(1:tot_vert) = new_vert_list(1:tot_vert)
                Endif

            ! death == False, only update vertex states
            ELSE
                Do i = 1, tot_vert
                    v = vert_list(i)
                    If (base_resource(v) >= RR(v)) THEN
                        vert_t(v) = vert_t(v) + 1
                        vertex_active(v) = 1
                        wastage(v) = wastage(v) + (base_resource(v)-RR(v))
                        If (unsafe(v) .eqv. .True.) THEN
                            tot_saved = tot_saved + 1
                        Endif
                    Else 
                        vertex_active(v) = 0
                    Endif
                Enddo                
                If (t == tot_iter .and. vertex_data .eqv. .True.) Then
                    ! note down vertex data 
                    Do i = 1, tot_vert
                        v = vert_list(i)
                        if (weighted .eqv. .False.) then
                            write(2, '(3(I0,A),I0)')v-1, ",", group_label(v), ",", deg_out(v), ",", deg_in(v)
                        else
                            write(2, '(2(I0,A),F0.3,A,F0.3)')v-1, ",", group_label(v), ",", deg_out_weights(v), ",", &
                                & deg_in_weights(v)
                        endif
                    Enddo
                Endif
            ENDIF

            ! Write time series of vertices
            IF (vertex_time_series .eqv. .True.) THEN
                write(fmt, '(A,I0,A)')"(", n-1, "(F0.3,','), F0.3)"
                write(7,fmt) base_resource
            ENDIF

            ! Increase the time index
            t = t + 1
        ENDDO
        IF (temporal .eqv. .True.) THEN
            close(1)
            close(3)
            close(4)
            If (directed .eqv. .True.) THEN
                close(5)
            Endif
        ENDIF
        IF (vertex_data .eqv. .True.) THEN
            close(2)
        ENDIF
        ! Free the memory occupied by the random number generator
        call free_rng(t)
    End subroutine vertex_dependency
end module resource_dependency 
!================================================================================
