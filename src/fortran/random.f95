!    This file is part of the package dependency-networks
!
!    Copyright (C) 2020-2021 Snehal Madhukar Shekatkar <snehal@inferred.in>
!
!    This package is free software: you can redistribute it and/or modify
!    it under the terms of the GNU General Public License as published by
!    the Free Software Foundation, either version 3 of the License, or
!    (at your option) any later version.
!
!    The package is distributed in the hope that it will be useful,
!    but WITHOUT ANY WARRANTY; without even the implied warranty of
!    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
!    GNU General Public License for more details.
!
!    You should have received a copy of the GNU General Public License
!    along with this program. If not, see <https://www.gnu.org/licenses/>.

!   This module contains the following subroutines
    
!   random_int : to generate random integers     
!   random_choice : to choose a random sample
!   random_shuffle : to randomly shuffle elements of an array
!   normal_random_number : to generate random numbers from normal distribution
!   exponential_random_number : to generate random numbers from exponential distribution

module random

    implicit none
    private :: pi
    real (kind=selected_real_kind(10,10)) :: pi = 3.1415926535


    interface random_choice
        module procedure random_choice_real_input
        module procedure random_choice_int_input
    end interface random_choice

    interface random_shuffle
        module procedure random_shuffle_real_input
        module procedure random_shuffle_int_input
    end interface random_shuffle

    contains

    !-----------------------------------------------------------------------

    subroutine random_int(low, high, val, prob, seed)

    ! Generate a uniform random integer between low and high (both inclusive)
    ! The generated value is stored in the variable 'val'
    ! Optinally, the array 'prob' can be supplied that contains the probability
    ! values with which to draw a random number from the range (low, high).

    implicit none
    integer, parameter :: int10 = selected_int_kind(10), real10 = selected_real_kind(10, 10)
    integer (4), optional :: seed(:)
    integer (kind=int10) :: i, n, low, high, val
    real (kind=int10), optional :: prob(high-low+1)
    real (kind=int10), allocatable :: x(:)
    real (kind=int10) :: a, b, r

    IF (present(seed)) THEN
        call random_seed(put=seed)
    ENDIF
    !call random_number(r)
    a = 0.0
    b = 1.0
    call uniform(r, a, b)
    IF (low == high) THEN
        val = low        
    ELSEIF (.not. present(prob))THEN
        val = low + floor((high+1-low)*r)
    ELSE
        n = high - low + 1
        allocate(x(n))
        x(1) = prob(1)
        DO i = 2, n
            x(i) = x(i-1) + prob(i)
        ENDDO
        DO i = 1, n
            If (r < x(i))Then
                val = low + i-1
                exit
            ENDIF
        ENDDO
        deallocate(x)
    ENDIF
    
    end subroutine random_int
!==========================================================================================

    subroutine random_choice_int_input(x, y, prob, replace, chosen_indices, state, seed)

    ! Generate a random sample from array x of size n
    ! If replace is .true., drawn value is kept in x
    ! prob is an array of size n that decides the probability
    ! with which individual elements are drawn

    implicit none
    integer, parameter :: int10 = selected_int_kind(10), real10 = selected_real_kind(10, 10)
    integer (4), optional :: seed(:)
    integer (kind=int10) :: i, j, n, last_ix, state
    integer (kind=int10) :: x(:), y(:), z(size(x))
    integer (kind=int10) :: range_array(size(x)), chosen_indices(size(y))
    real (kind=real10) :: norm
    real (kind=real10), optional :: prob(size(x))
    real (kind=real10), allocatable :: prob_dummy(:), dynamic_prob(:)
    logical, optional :: replace
    logical :: replace_dummy
    intent(inout) :: state

    n = size(x)
    IF (present(replace)) THEN
        replace_dummy = replace
    ELSE
        replace_dummy = .True.
    ENDIF
    IF (present(seed)) THEN
        call random_seed(put=seed)
    ENDIF
    IF (.not. present(prob)) THEN
        allocate(prob_dummy(n))
        prob_dummy = real(1.0, 8)/n
    ELSE
        prob_dummy = prob
    ENDIF
    IF (replace_dummy .eqv. .True.) THEN
        DO i = 1, size(y)
            call random_int(int(1, 8), int(size(x), 8), j, prob=prob_dummy)
            y(i) = x(j)
            chosen_indices(i) = j
        ENDDO
    ELSE
        If (any(prob_dummy < 1.0D-8))Then
            state = 1
        Elseif (abs(sum(prob_dummy) - 1.0) > 1.0D-6)Then
            state = 2
        Elseif (any(prob_dummy < 0.0))Then
            state = 3
        Else
            z = x
            DO i = 1, size(y)
                range_array(i) = i
            ENDDO
            if (.not. present(prob))then
                DO i = 1, size(y)
                    last_ix = int(size(z)-i+1, 8)
                    call random_int(int(1, 8), last_ix, j)
                    y(i) = z(j)
                    chosen_indices(i) = range_array(j)
                    ! Overwrite the j'th element of z and range_array by their last elements
                    z(j) = z(size(z)-i+1)
                    range_array(j) = range_array(last_ix)
                ENDDO
            else
                allocate(dynamic_prob(size(prob_dummy)))
                dynamic_prob = prob_dummy
                DO i = 1, size(y)
                    last_ix = int(size(z)-i+1, 8)
                    call random_int(int(1, 8), last_ix, j, prob=dynamic_prob(1:last_ix))
                    y(i) = z(j)
                    chosen_indices(i) = range_array(j)
                    ! Overwrite the j'th element of z and range_array by their last elements
                    z(j) = z(last_ix)
                    range_array(j) = range_array(last_ix)
                    ! Recalculate the probabilities
                    dynamic_prob(j) = dynamic_prob(last_ix)
                    norm = sum(dynamic_prob(1:last_ix-1))
                    dynamic_prob(1:last_ix-1) = dynamic_prob(1:last_ix-1) / norm
                ENDDO
                deallocate(dynamic_prob)
            endif
            state = 0
        Endif
        
    ENDIF
        
    end subroutine random_choice_int_input
!==========================================================================================

    subroutine random_choice_real_input(x, y, prob, replace, chosen_indices, state, seed)

    ! Generate a random sample from array x of size n
    ! If replace is .true., drawn value is kept in x
    ! prob is an array of size n that decides the probability
    ! with which individual elements are drawn

    implicit none
    integer, parameter :: int10 = selected_int_kind(10), real10 = selected_real_kind(10, 10)
    integer (4), optional :: seed(:)
    integer (kind=int10) :: i, j, n, state, last_ix
    real (kind=real10) :: norm, x(:), y(:), z(size(x))
    integer (kind=int10) :: range_array(size(x)), chosen_indices(size(y))
    real (kind=real10), optional :: prob(size(x))
    real (kind=real10), allocatable :: prob_dummy(:), dynamic_prob(:)
    logical, optional :: replace
    logical :: replace_dummy
    intent(inout) :: state

    n = size(x)
    IF (present(replace)) THEN
        replace_dummy = replace
    ELSE
        replace_dummy = .True.
    ENDIF
    IF (present(seed)) THEN
        call random_seed(put=seed)
    ENDIF
    IF (.not. present(prob)) THEN
        allocate(prob_dummy(n))
        prob_dummy = real(1.0, 8)/n
    ELSE
        prob_dummy = prob
    ENDIF
    IF (replace_dummy .eqv. .True.) THEN
        DO i = 1, size(y)
            call random_int(int(1, 8), int(size(x), 8), j, prob=prob_dummy)
            y(i) = x(j)
            chosen_indices(i) = j
        ENDDO
    ELSE
        z = x
        if (.not. present(prob))then
            DO i = 1, size(y)
                range_array(i) = i
            ENDDO
            DO i = 1, size(y)
                call random_int(int(1, 8), int(size(z)-i+1, 8), j)
                y(i) = z(j)
                chosen_indices(i) = range_array(j)
                ! Overwrite the j'th element of z and range_array by their last elements
                z(j) = z(size(z)-i+1)
                range_array(j) = range_array(size(z)-i+1)
            ENDDO
        else
            allocate(dynamic_prob(size(prob_dummy)))
            dynamic_prob = prob_dummy
            DO i = 1, size(x)
                range_array(i) = i
            ENDDO
            DO i = 1, size(y)
                last_ix = int(size(z)-i+1, 8)
                call random_int(int(1, 8), last_ix, j, prob=dynamic_prob(1:last_ix))
                y(i) = z(j)
                chosen_indices(i) = range_array(j)
                ! Overwrite the j'th element of z and range_array by their last elements
                z(j) = z(last_ix)
                range_array(j) = range_array(last_ix)
                ! Recalculate the probabilities
                dynamic_prob(j) = dynamic_prob(last_ix)
                norm = sum(dynamic_prob(1:last_ix-1))
                dynamic_prob(1:last_ix-1) = dynamic_prob(1:last_ix-1) / norm
            ENDDO
            deallocate(dynamic_prob)
        endif
        state = 0
    ENDIF
        
    end subroutine random_choice_real_input
!==========================================================================================

    subroutine random_shuffle_int_input(x, shuffled_indices, shuffling_indices, prob, seed)

    ! Randomly shuffle the array x
    ! x has type integer (kind=8)
    ! shuffled_indices should (ideally) contain integers 1, 2, ..., n in that order 
    ! so that finally its permutation that indicates actual shuffling will be returned
    ! If 'prob' is present, the entries corresponding to higher probabilities will tend 
    ! to be towards the left of the shuffled array

    implicit none
    integer, parameter :: int10 = selected_int_kind(10), real10 = selected_real_kind(10, 10)
    integer (4), optional :: seed(:)
    integer (kind=int10) :: i, n, state, x(:)
    integer (kind=int10), optional :: shuffled_indices(:), shuffling_indices(:)
    integer (kind=int10), allocatable :: y(:)
    integer (kind=int10), allocatable :: chosen_indices(:)
    real (kind=real10), optional :: prob(:)
    real (kind=real10) :: prob_dummy(size(x))

    n = size(x)
    allocate(y(n), chosen_indices(n))
    IF (present(prob)) THEN
        prob_dummy = prob
    ELSE
        prob_dummy = 1.0/n
    ENDIF
    IF (present(shuffling_indices)) THEN
        DO i = 1, n
            y(i) = x(shuffling_indices(i))
        ENDDO
        If (present(shuffled_indices)) Then
            shuffled_indices = shuffling_indices
        Endif
    ELSE
        If (present(seed)) Then
            call random_choice(x, y, prob=prob_dummy, replace=.False., chosen_indices=chosen_indices, & 
                & state=state, seed=seed)
        Else
            call random_choice(x, y, prob=prob_dummy, replace=.False., chosen_indices=chosen_indices, state=state)
        Endif
        If (present(shuffled_indices)) Then
            shuffled_indices = shuffling_indices
        Endif
    ENDIF

    x = y
    deallocate(y, chosen_indices)

    end subroutine random_shuffle_int_input

!==========================================================================================
    subroutine random_shuffle_real_input(x, shuffled_indices, shuffling_indices, prob, seed)

    ! Randomly shuffle the array x
    ! x has type real (kind=8)

    implicit none
    integer, parameter :: int10 = selected_int_kind(10), real10 = selected_real_kind(10, 10)
    integer (4), optional :: seed(:)
    integer (kind=int10) :: i, n, state
    integer (kind=int10), optional :: shuffled_indices(:), shuffling_indices(:)
    real (kind=real10) :: x(:), prob_dummy(size(x))
    real (kind=real10), allocatable :: y(:)
    integer (kind=real10), allocatable :: chosen_indices(:)
    real (kind=real10), optional :: prob(:)

    n = size(x)
    allocate(y(n), chosen_indices(n))
    IF (present(prob)) THEN
        prob_dummy = prob
    ELSE
        prob_dummy = 1.0/n
    ENDIF
    IF (present(shuffling_indices)) THEN
        DO i = 1, n
            y(i) = x(shuffling_indices(i))
        ENDDO
        If (present(shuffled_indices)) Then
            shuffled_indices = shuffling_indices
        Endif
    ELSE
        If (present(seed)) Then
            call random_choice(x, y, prob=prob_dummy, replace=.False., chosen_indices=chosen_indices, & 
                & state=state, seed=seed)
        Else
            call random_choice(x, y, prob=prob_dummy, replace=.False., chosen_indices=chosen_indices, state=state)
        Endif
        If (present(shuffled_indices)) Then
            shuffled_indices = chosen_indices
        Endif
    ENDIF

    x = y
    deallocate(y, chosen_indices)

    end subroutine random_shuffle_real_input
!==========================================================================================

    subroutine normal_random_number(x, mu, sigma, uniform_r1, uniform_r2)
    
    ! Return a random number from the Gaussian distribution with mean mu and variance sigma**2
    ! The method uses Box-Muller transformation

    implicit none
    integer, parameter :: int10 = selected_int_kind(10), real10 = selected_real_kind(10, 10)
    real (kind=real10), optional :: uniform_r1, uniform_r2
    real (kind=real10) :: r1, r2, mu, sigma, x

    IF ((.not. present(uniform_r1)) .and. (.not. present(uniform_r2))) THEN
        call random_number(r1)
        call random_number(r2)
    ELSE 
        r1 = uniform_r1
        r2 = uniform_r2
    ENDIF

    x = sqrt(-2*log(r1)) * cos(2*pi*r2)
    x = mu + sigma*x

    end subroutine normal_random_number 
!==========================================================================================

    subroutine exponential_random_number(x, beta, uniform_r)

    ! Generate random numbers from the Exponential distribution with mean beta

    implicit none
    integer, parameter :: int10 = selected_int_kind(10), real10 = selected_real_kind(10, 10)
    !integer(4), optional :: seed(:)
    real (kind=real10), optional :: uniform_r
    real (kind=real10) :: r, beta, x

    IF (.not. present(uniform_r)) THEN
        call random_number(r)
    ELSE 
        r = uniform_r
    ENDIF
    x = -beta * log(r)

    end subroutine exponential_random_number
!==========================================================================================
end module random
