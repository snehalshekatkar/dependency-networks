!    This file is part of the package dependency-networks
!
!    Copyright (C) 2020-2021 Snehal Madhukar Shekatkar <snehal@inferred.in>
!
!    This package is free software: you can redistribute it and/or modify
!    it under the terms of the GNU General Public License as published by
!    the Free Software Foundation, either version 3 of the License, or
!    (at your option) any later version.
!
!    The package is distributed in the hope that it will be useful,
!    but WITHOUT ANY WARRANTY; without even the implied warranty of
!    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
!    GNU General Public License for more details.
!
!    You should have received a copy of the GNU General Public License
!    along with this program. If not, see <https://www.gnu.org/licenses/>.

module delete_vertex

    implicit none

    contains 

    !-------------------------------------------------------------------------
    subroutine update_adjacency_lists(v, directed, deg_out, deg_out_temp, adj_list_out, & 
        & weights_adj_list_out, weights_adj_list_in, deg_in, deg_in_temp, adj_list_in)

        implicit none
        integer, parameter :: int10 = selected_int_kind(10), real10 = selected_real_kind(10, 10)
        logical :: directed
        integer (kind=int10) :: counter, j, k, v, nbr
        integer (kind=int10) :: deg_out(:), deg_out_temp(:), adj_list_out(:,:)
        integer (kind=int10) :: deg_in(:), deg_in_temp(:), adj_list_in(:,:)
        real (kind=real10) :: weights_adj_list_out(:,:), weights_adj_list_in(:,:)

        If (directed .eqv. .False.) Then
            ! The graph is undirected, delete v from adj_list_out of nbrs of v 
            del_v: do j = 1, deg_out_temp(v)
                nbr = adj_list_out(j, v) 
                if (nbr /= v) then
                    counter = 0
                    do k = 1, deg_out_temp(nbr)
                        ! if there is a multiedge, delete all occurrences of v
                        do while (adj_list_out(k, nbr) == v)
                            if (k > deg_out_temp(nbr)-counter) then
                                exit
                            endif
                            adj_list_out(k, nbr) = adj_list_out(deg_out_temp(nbr)-counter, nbr)
                            weights_adj_list_out(k, nbr) = weights_adj_list_out(deg_out_temp(nbr)-counter, nbr)
                            counter = counter + 1
                        enddo
                    enddo
                    ! Update the temporary in and out-degrees 
                    deg_out_temp(nbr) = deg_out_temp(nbr) - counter
                    deg_in_temp(nbr) = deg_out_temp(nbr) ! the graph is undirected
                endif
            enddo del_v
            deg_out_temp(v) = 0
            deg_in_temp(v) = 0
            adj_list_out(:, v) = -1
            adj_list_in(:, v) = -1
            weights_adj_list_out(:, v) = -1
            weights_adj_list_in(:, v) = -1
        Else
            ! The graph is directed, delete v from adj_list_in of out nbrs of v
            ! and from adj_list_out of in nbrs of v
            del_v_out_nbrs: do j = 1, deg_out_temp(v) ! delete v from adj_list_in of out-neighbours
                nbr = adj_list_out(j, v) 
                if (nbr /= v) then
                    counter = 0
                    do k = 1, deg_in_temp(nbr)
                        ! if there is a multiedge, delete all occurrences of v
                        do while (adj_list_in(k, nbr) == v)
                            if (k > deg_in_temp(nbr)-counter) then
                                exit
                            endif
                            ! overwrite v by last available entry in the list
                            adj_list_in(k, nbr) = adj_list_in(deg_in_temp(nbr)-counter, nbr)
                            weights_adj_list_in(k, nbr) = weights_adj_list_in(deg_in_temp(nbr)-counter, nbr)
                            counter = counter + 1
                        enddo
                    enddo
                    ! Update the temporary in-degree of the neighbour
                    deg_in_temp(nbr) = deg_in_temp(nbr) - counter
                endif
            enddo del_v_out_nbrs
            deg_out_temp(v) = 0
            adj_list_out(:, v) = -1
            weights_adj_list_out(:, v) = -1
            del_v_in_nbrs: do j = 1, deg_in_temp(v) ! delete v from adj_list_out of in-neighbours
                nbr = adj_list_in(j, v) 
                if (nbr /= v) then
                    counter = 0
                    do k = 1, deg_out_temp(nbr)
                        ! if there is a multiedge, delete all occurrences of v
                        do while (adj_list_out(k, nbr) == v)
                            if (k > deg_out_temp(nbr)-counter) then
                                exit
                            endif
                            adj_list_out(k, nbr) = adj_list_out(deg_out_temp(nbr)-counter, nbr)
                            weights_adj_list_out(k, nbr) = weights_adj_list_out(deg_out_temp(nbr)-counter, nbr)
                            counter = counter + 1
                        enddo
                    enddo
                    ! Update the temporary out-degree of the neighbour
                    deg_out_temp(nbr) = deg_out_temp(nbr) - counter
                endif
            enddo del_v_in_nbrs
            deg_in_temp(v) = 0
            adj_list_in(:, v) = -1
            weights_adj_list_in(:, v) = -1
        Endif
    end subroutine update_adjacency_lists

end module delete_vertex
