!    This file is part of the package dependency-networks
!
!    Copyright (C) 2020-2021 Snehal Madhukar Shekatkar <snehal@inferred.in>
!
!    This package is free software: you can redistribute it and/or modify
!    it under the terms of the GNU General Public License as published by
!    the Free Software Foundation, either version 3 of the License, or
!    (at your option) any later version.
!
!    The package is distributed in the hope that it will be useful,
!    but WITHOUT ANY WARRANTY; without even the implied warranty of
!    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
!    GNU General Public License for more details.
!
!    You should have received a copy of the GNU General Public License
!    along with this program. If not, see <https://www.gnu.org/licenses/>.

module sorting
    implicit none

    ! Interface for accepting either real or integer array as an input
    interface mergesort
        module procedure mergesort_real_input
        module procedure mergesort_int_input
    end interface mergesort 

    contains

    !----------------------------------------------------------------------------------------
    subroutine range_array(x, low, high)

    ! Return an integer array x containing consecutive integers between low and high (both inclusive)

    implicit none
    integer, parameter :: int10 = selected_int_kind(10), real10 = selected_real_kind(10,10)
    integer (kind=int10) :: i, high, x(:)
    integer (kind=int10), optional :: low


    IF (.not. present(low)) THEN
        DO i = 1, high
            x(i) = i
        ENDDO
    ELSE
        DO i = low, high
            x(i) = i
        ENDDO
    ENDIF

    end subroutine range_array
!==========================================================================================

    recursive subroutine mergesort_real_input(n, x, sorting_indices)

    ! Sort an array x of size n using mergesort algorithm
    ! This routine operates only on real array (for integer array, use the routine mergesort_int_input given below)
    ! The input value of the array sorting_indices must be (/1, 2, ..., n/)
    ! The returned value of this array contains indices which would sort the array sorting_indices

    implicit none
    integer, parameter :: int10 = selected_int_kind(10), real10 = selected_real_kind(10,10)
    integer (kind=int10) :: n, middle, sorting_indices(n), temp_int
    real (kind=real10) :: temp, x(n)
    real (kind=real10), allocatable :: left_half(:), right_half(:)
    integer (kind=int10), allocatable :: sorting_indices_left_half(:), sorting_indices_right_half(:)

    IF (n == 1) THEN
        continue
    ELSE
        continue
        If (n == 2) Then
            if (x(1) > x(2)) then
                ! Swap the two values
                temp = x(1)
                x(1) = x(2)
                x(2) = temp
                temp_int = sorting_indices(1)
                sorting_indices(1) = sorting_indices(2)
                sorting_indices(2) = temp_int
            endif
        Else
            ! Divide the array x into two halves
            if (mod(n, 2) == 1) Then
                middle = 1 + (n-1)/2
            else
                middle = n / 2
            endif
            allocate(left_half(middle), right_half(n-middle))
            allocate(sorting_indices_left_half(middle), sorting_indices_right_half(n-middle))
            left_half = x(1:middle)
            right_half = x(middle+1 : n)
            sorting_indices_left_half = sorting_indices(1:middle)
            sorting_indices_right_half = sorting_indices(middle+1 : n)
            ! Now apply mergesort on the two halves recursively
            call mergesort_real_input(int(size(left_half), int10), left_half, sorting_indices_left_half)
            call mergesort_real_input(int(size(right_half), int10), right_half, sorting_indices_right_half)
            ! Merge the two sorted halves
            call merge_halves_real_input(left_half, right_half, sorting_indices_left_half, sorting_indices_right_half, & 
                & x, sorting_indices)
            deallocate(left_half, right_half, sorting_indices_left_half, sorting_indices_right_half)
        Endif
    ENDIF

    end subroutine mergesort_real_input
!==========================================================================================

    subroutine merge_halves_real_input(left_half, right_half, sorting_indices_left_half, sorting_indices_right_half, &
        & merged_array, sorting_indices)

    ! Merge two sorted arrays so that the merged array is sorted
    ! This routine operates only on real arrays (for integer arrays, use the routine merge_halves_int_input given below)
    ! This routine is called by the mergesort_real_input routine

    implicit none
    integer, parameter :: int10 = selected_int_kind(10), real10 = selected_real_kind(10,10)
    integer (kind=int10) :: i, n, left_ptr, right_ptr
    real (kind=real10) :: left_half(:), right_half(:), merged_array(:)
    integer (kind=int10) :: sorting_indices_left_half(:), sorting_indices_right_half(:), sorting_indices(:)

    ! Merge the left and right halves so that the merged array is sorted
    left_ptr = 1
    right_ptr = 1
    n = size(left_half) + size(right_half)
    DO i = 1, n
        If (left_ptr-1 == size(left_half)) Then
            merged_array(i:n) = right_half(right_ptr : size(right_half))
            sorting_indices(i:n) = sorting_indices_right_half(right_ptr : size(right_half))
            exit
        Elseif (right_ptr-1 == size(right_half)) Then
            merged_array(i:n) = left_half(left_ptr : size(left_half))
            sorting_indices(i:n) = sorting_indices_left_half(left_ptr : size(left_half))
            exit
        Elseif (left_half(left_ptr) <= right_half(right_ptr)) Then
            merged_array(i) = left_half(left_ptr)
            sorting_indices(i) = sorting_indices_left_half(left_ptr)
            left_ptr = left_ptr + 1
        Else
            merged_array(i) = right_half(right_ptr)
            sorting_indices(i) = sorting_indices_right_half(right_ptr)
            right_ptr = right_ptr + 1
        Endif
    ENDDO

    end subroutine merge_halves_real_input
!==========================================================================================

    recursive subroutine mergesort_int_input(n, x, sorting_indices)

    ! Sort an array x of size n using mergesort algorithm
    ! This routine operates only on integer array (for real array, use the routine mergesort_real_input given above)
    ! The input value of the array sorting_indices must be (/1, 2, ..., n/)
    ! The returned value of this array contains indices which would sort the array sorting_indices

    implicit none
    integer, parameter :: int10 = selected_int_kind(10), real10 = selected_real_kind(10,10)
    integer (kind=int10) :: n, middle, sorting_indices(n)
    integer (kind=int10) :: temp, x(n)
    integer (kind=int10), allocatable :: left_half(:), right_half(:)
    integer (kind=int10), allocatable :: sorting_indices_left_half(:), sorting_indices_right_half(:)

    IF (n == 1) THEN
        continue
    ELSE
        continue
        If (n == 2) Then
            if (x(1) > x(2)) then
                ! Swap the two values
                temp = x(1)
                x(1) = x(2)
                x(2) = temp
                temp = sorting_indices(1)
                sorting_indices(1) = sorting_indices(2)
                sorting_indices(2) = temp
            endif
        Else
            ! Divide the array x into two halves
            if (mod(n, 2) == 1) Then
                middle = 1 + (n-1)/2
            else
                middle = n / 2
            endif
            allocate(left_half(middle), right_half(n-middle))
            allocate(sorting_indices_left_half(middle), sorting_indices_right_half(n-middle))
            left_half = x(1:middle)
            right_half = x(middle+1 : n)
            sorting_indices_left_half = sorting_indices(1:middle)
            sorting_indices_right_half = sorting_indices(middle+1 : n)
            ! Now apply mergesort on the two halves recursively
            call mergesort_int_input(int(size(left_half), int10), left_half, sorting_indices_left_half)
            call mergesort_int_input(int(size(right_half), int10), right_half, sorting_indices_right_half)
            ! Merge the two sorted halves
            call merge_halves_int_input(left_half, right_half, sorting_indices_left_half, &
                & sorting_indices_right_half, x, sorting_indices)
            deallocate(left_half, right_half, sorting_indices_left_half, sorting_indices_right_half)
        Endif
    ENDIF

    end subroutine mergesort_int_input
!==========================================================================================

    subroutine merge_halves_int_input(left_half, right_half, sorting_indices_left_half, sorting_indices_right_half, &  
        & merged_array, sorting_indices)

    ! Merge two sorted arrays so that the merged array is sorted
    ! This routine operates only on real arrays (for integer arrays, use the routine merge_halves_int_input given below)
    ! This routine is called by the mergesort_int_input routine

    implicit none
    integer, parameter :: int10 = selected_int_kind(10), real10 = selected_real_kind(10,10)
    integer (kind=int10) :: i, n, left_ptr, right_ptr
    integer (kind=int10) :: left_half(:), right_half(:), merged_array(:)
    integer (kind=int10) :: sorting_indices_left_half(:), sorting_indices_right_half(:), sorting_indices(:)

    ! Merge the left and right halves so that the merged array is sorted
    left_ptr = 1
    right_ptr = 1
    n = size(left_half) + size(right_half)
    DO i = 1, n
        If (left_ptr-1 == size(left_half)) Then
            merged_array(i:n) = right_half(right_ptr : size(right_half))
            sorting_indices(i:n) = sorting_indices_right_half(right_ptr : size(right_half))
            exit
        Elseif (right_ptr-1 == size(right_half)) Then
            merged_array(i:n) = left_half(left_ptr : size(left_half))
            sorting_indices(i:n) = sorting_indices_left_half(left_ptr : size(left_half))
            exit
        Elseif (left_half(left_ptr) <= right_half(right_ptr)) Then
            merged_array(i) = left_half(left_ptr)
            sorting_indices(i) = sorting_indices_left_half(left_ptr)
            left_ptr = left_ptr + 1
        Else
            merged_array(i) = right_half(right_ptr)
            sorting_indices(i) = sorting_indices_right_half(right_ptr)
            right_ptr = right_ptr + 1
        Endif
    ENDDO

    end subroutine merge_halves_int_input
!==========================================================================================

End module sorting
