
!    This file is part of the package dependency-networks
!
!    Copyright (C) 2020-2021 Snehal Madhukar Shekatkar <snehal@inferred.in>
!
!    This package is free software: you can redistribute it and/or modify
!    it under the terms of the GNU General Public License as published by
!    the Free Software Foundation, either version 3 of the License, or
!    (at your option) any later version.
!
!    The package is distributed in the hope that it will be useful,
!    but WITHOUT ANY WARRANTY; without even the implied warranty of
!    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
!    GNU General Public License for more details.
!
!    You should have received a copy of the GNU General Public License
!    along with this program. If not, see <https://www.gnu.org/licenses/>.

module request_offer
    use auxiliary
    use random
    use sorting
    use topology
    use delete_vertex

    implicit none

    contains
!==============================================================================================================
    subroutine request_offer_model(n, death, tot_iter, temporal, temporal_lc, &
        & temporal_resol, vertex_time_series, deg_out, deg_in, adj_list_out,  &
        & adj_list_in, multigraph, weighted, weights_adj_list_out,            &
        & weights_adj_list_in, beta, RR, eta, prob_dist, money,               &
        & unit_resource_price, strategy, temp_summary_file,                   &
        & vertex_data, vertex_file, money_data, money_file, reqoff_count,     &
        & seed, use_seed, vertex_time_series_file, tot_vulnerable, tot_saved, &
        & money_deficient_verts, tot_resource_wastage, vert_t)

        implicit none
        logical :: death, vertex_data, money_data, multigraph, weighted
        logical :: temporal, temporal_lc, use_seed, directed, vertex_time_series
        character (len=100) :: strategy, temp_summary_file, vertex_file, money_file, prob_dist, fmt
        character (len=100) :: vertex_time_series_file
        integer, parameter :: int10 = selected_int_kind(10), real10 = selected_real_kind(10, 10)
        integer (int10) :: n, money_deficient_verts, reqoff_count
        integer (int10) :: deg_out(:), deg_in(:), adj_list_out(:, :), adj_list_in(:, :)
        real (real10)   :: r, beta(:, :), RR(:), eta(:), weights_adj_list_out(:,:), weights_adj_list_in(:,:)
        real (real10)   :: unit_resource_price, money(:)

        logical         :: death_condition, activity_condition, is_vulnerable(n)
        integer (int10) :: i, j, k, v, t, ix, nbr, tot_vert, max_out_deg, state, tot_vulnerable, tot_saved, tot_iter
        integer (int10) :: temporal_resol, largest_comp_size, largest_comp_edges, counter
        integer (int10) :: money_deficient_verts_t, seed, m1, m2
        real (real10)   :: numerator, denominator, money_ave, money_std, max_money, min_money
        integer (int10) :: component(n), comp_vert(n), comp_edges(n), vert_t(n), tot_times_saved(n), money_deficiency(n)
        integer (int10) :: vertex_active(n), tot_requesting_nbrs(n), tot_offers_received(n), vert_list(n)
        integer (int10) :: deg_out_temp(n), deg_in_temp(n)
        integer (int10) :: tot_requests_sent_lifetime(n), tot_offers_received_lifetime(n)
        real (real10) :: deg_out_weights(n), deg_out_temp_weights(n), deg_in_weights(n), deg_in_temp_weights(n)
        real (real10) :: base_resource(n), temp_resource(n), surplus(n), init_money(n), resource_wastage(n)
        real (real10) :: largest_comp_link_density, ave_deg_out, norm, tot_resource_wastage

        integer (int10), allocatable :: shuffled_indices(:), sorting_indices(:), chosen_indices(:)
        integer (int10), allocatable :: requesting_nbrs(:, :), offering_nbrs(:, :)
        real (real10), allocatable :: requested_amounts(:, :), offered_amounts(:, :)
        real (real10), allocatable :: prob(:), dummy_array(:)

!f2py   intent(in) :: n, deg_out, beta, RR, eta, money, unit_resource_price, strategy
!f2py   intent(in) :: weights_adj_list_out, weights_adj_list_in, adj_list_out, prob_dist
!f2py   intent(in) :: deg_in, adj_list_in
!f2py   intent(in) :: temporal, temporal_lc, temporal_resol, activity
!f2py   intent(in) :: tot_iter, use_seed, reqoff_count
!f2py   intent(in) :: temp_summary_file, vertex_file, money_file, vertex_data, money_data
!f2py   intent(out) :: tot_vulnerable, tot_saved, money_deficient_verts, tot_resource_wastage, vert_t

        IF (use_seed .eqv. .False.) THEN
            ! Generate a random integer between 2 and 100000000 
            ! to be used as a seed for GSL functions
            m1 = 2; m2 = 100000000
            call random_number(r)
            seed = m1 + FLOOR(r*(m2-m1+1))
        ENDIF
        ! Initialize the random number generator and set seed for it
        call initialize_rng(seed)
        IF (temporal .eqv. .True.)then
            open(unit=1, file=temp_summary_file)
            If (temporal_lc .eqv. .True.) Then
                write(1, '(A)')"time,network_size,largest_comp_size,largest_comp_link_density,ave_deg,money_ave,&
                &money_std,max_money,min_money,money_deficient_verts"
            Else
                write(1, '(A)')"time,network_size,ave_deg,money_ave,money_std,max_money,min_money,money_deficient_verts"
            Endif
            If (money_data .eqv. .True.) Then
                open(unit=3, file=money_file)
            Endif
        ENDIF

        ! Vertex data
        If (vertex_data .eqv. .True.) Then
            open(unit=2, file=vertex_file)
            if (death .eqv. .True.) then
                write(2, '(A)')"vert_index,deg_out,deg_out_temp,deg_in,deg_in_temp,time,tot_requests_sent,&
                &tot_offers_received,money_deficiency,tot_times_saved,init_money,final_money,resource_wastage"
            else
                write(2, '(A)')"vert_index,deg_out,deg_in,tot_requests_sent,tot_offers_received,&
                &money_deficiency,tot_times_saved,init_money,final_money,resource_wastage"
            endif
        Endif
        ! Vertex time series file
        IF (vertex_time_series .eqv. .True.) THEN
            open(unit=4, file=vertex_time_series_file)           
        ENDIF

        ! Initialization
        tot_vulnerable = 0
        tot_saved = 0
        tot_vert = n
        money_deficient_verts = 0
        money_deficient_verts_t = 0
        max_out_deg = maxval(deg_out)
        DO i = 1, n
            vert_list(i) = i
        ENDDO
        deg_out_temp = deg_out
        deg_in_temp = deg_in
        init_money = money
        tot_requests_sent_lifetime = 0
        tot_offers_received_lifetime = 0
        resource_wastage = 0
        tot_resource_wastage = 0
        money_deficiency = 0
        tot_times_saved = 0
        t = 1
        allocate(requesting_nbrs(max_out_deg, n), requested_amounts(max_out_deg, n))
        allocate(offering_nbrs(max_out_deg, n), offered_amounts(max_out_deg, n))
        allocate(shuffled_indices(max_out_deg), sorting_indices(max_out_deg), chosen_indices(max_out_deg))
        allocate(dummy_array(max_out_deg), prob(max_out_deg))

        ! If the graph is weighted, save the sum of the edge weights around each vertex
        IF (weighted .eqv. .True.) then
            DO i = 1, n
                v = vert_list(i)
                deg_out_weights(v) = sum(weights_adj_list_out(1:deg_out(v), v))
                deg_in_weights(v) = sum(weights_adj_list_in(1:deg_in(v), v))
            ENDDO
        ENdIF

        ! Define death and activity conditions to decide whether vertices die or not
        death_condition    = (death .eqv. .True.) .and. (tot_vert > 0)
        activity_condition = (death .eqv. .False.) .and. (t < tot_iter)

        time_loop: DO WHILE (death_condition .or. activity_condition)
            ! Update death and activity conditions
            death_condition    = (death .eqv. .True.) .and. (tot_vert > 0)
            activity_condition = (death .eqv. .False.) .and. (t < tot_iter)

            deg_out_temp_weights = 0
            deg_in_temp_weights = 0

            IF (temporal .eqv. .True. .and. mod(t, temporal_resol) == 0) THEN
                    ! Calculate the average degree
                    If (sum(deg_out_temp) > 0)Then
                        if (weighted .eqv. .False.) then
                            ave_deg_out = real(sum(deg_out_temp), 8) / tot_vert
                        else
                            ave_deg_out = 0.0
                            Do i = 1, tot_vert
                                v = vert_list(i)
                                ave_deg_out = ave_deg_out + sum(weights_adj_list_out(1:deg_out_temp(v), v))
                            Enddo
                            ave_deg_out = ave_deg_out / tot_vert
                        endif
                    Else
                        ave_deg_out = 0 
                    Endif

                    ! Calculate the standard deviation of the money and total money
                    money_std = 0
                    money_ave = 0
                    max_money = minval(money) ! There is no bug on this line. Values are updated in the DO loop
                    min_money = maxval(money) ! There is no bug on this line. Values are updated in the DO loop
                    Do i = 1, tot_vert
                        v = vert_list(i)
                        money_ave = money_ave + money(v)
                        if (money(v) > max_money) max_money = money(v)
                        if (money(v) < min_money) min_money = money(v)
                    Enddo
                    money_ave = money_ave / tot_vert
                    If (money_data .eqv. .True.) Then
                        write(fmt,'("(", I0,"(F0.3,A)",")")')tot_vert
                        write(3, fmt)(money(vert_list(i)),",", i=1,tot_vert)
                    Endif
                    Do i = 1, tot_vert
                        v = vert_list(i)
                        money_std = money_std + (money(v)-money_ave)**2
                    Enddo
                    If (tot_vert > 1) Then
                        money_std = sqrt(money_std/(tot_vert-1))
                    Else
                        money_std = 0
                    Endif
                    If (temporal_lc .eqv. .False.) Then
                        write(1, '(2(I0,A),4(F0.3,A),F0.3,A,I0)') t, ",", tot_vert, ",", ave_deg_out, ",", money_ave, ",", & 
                            & money_std, ",", max_money, ",", min_money, ",", money_deficient_verts_t
                    Else
                    ! Find the largest component and compute its characteristics
                        call BFS(n, max_out_deg, adj_list_out, deg_out_temp, component, comp_vert, comp_edges)
                        ! Get the number of vertices and edges in the largest component
                        ix = maxloc(comp_vert, dim = 1) ! Get the index of a vertex 
                                                        ! that belongs to the largest 
                                                        ! component
                        largest_comp_size = comp_vert(ix)
                        largest_comp_edges = comp_edges(ix)
                        ! Calculate the link density of the largest component
                        numerator = 2. * real(largest_comp_edges, 8) 
                        denominator = largest_comp_size * (largest_comp_size-1)
                        if (denominator > 0) then
                            largest_comp_link_density = numerator / denominator
                        endif
                        write(1, '(3(I0,A),4(F0.3,A),F0.3,A,I0)') t, ",", tot_vert, ",", largest_comp_size, ",", & 
                            & largest_comp_link_density, ",", ave_deg_out, ",", money_ave, ",", money_std, &
                            & ",", money_deficient_verts_t
                    Endif
            ENDIF
            is_vulnerable = .False.
            ! Generate base resources
            temp_resource = 0
            Do i = 1, tot_vert
                v = vert_list(i)
                IF (prob_dist == 'uniform') THEN
                    call uniform(base_resource(v), beta(1,v), beta(2,v))
                ELSEIF (prob_dist == 'exponential') THEN
                    call exponential(base_resource(v), beta(1,v))
                ELSEIF (prob_dist == 'gaussian') THEN
                    call gaussian(base_resource(v), beta(1,v), beta(2,v))
                ELSEIF (prob_dist == 'gaussian_positive') THEN
                    call gaussian_positive(base_resource(v), beta(1,v), beta(2,v))
                ELSEIF (prob_dist == 'pareto') THEN
                    call pareto(base_resource(v), beta(1, v), beta(2, v))
                ELSEIF (prob_dist == 'chi') THEN
                    call chi(base_resource(v), beta(1, v))
                ELSEIF (prob_dist == 'chi_squared') THEN
                    call chi_squared(base_resource(v), beta(1, v))
                ELSEIF (prob_dist == 'logistic') THEN
                    call logistic(base_resource(v), beta(1, v), beta(2, v))
                ELSEIF (prob_dist == 'logistic_positive') THEN
                    call logistic_positive(base_resource(v), beta(1, v), beta(2, v))
                ELSEIF (prob_dist == 'gamma') THEN
                    call gamma_dist(base_resource(v), beta(1, v), beta(2, v))
                ENDIF
                IF (base_resource(v) < RR(v)) THEN
                    is_vulnerable(v) = .True.
                    tot_vulnerable = tot_vulnerable + 1
                ENDIF
            Enddo

            ! Send out requests to neighbours
            tot_requesting_nbrs = 0
            money_deficient_verts_t = 0
            ! The arrays requesting_nbrs and requested_amounts need not be re-initialized
            ! In fact, initializing them significantly slows down the code
            request_loop: Do i = 1, tot_vert
                v = vert_list(i)
                IF (base_resource(v) < RR(v) .and. money(v) >= unit_resource_price*(RR(v)-base_resource(v))) THEN
                    ! Send requests to neighbours (or in-neighbours in case of directed net)
                    do j = 1, deg_in_temp(v)
                        nbr = adj_list_in(j, v)
                        tot_requesting_nbrs(nbr) = tot_requesting_nbrs(nbr) + 1
                        requesting_nbrs(tot_requesting_nbrs(nbr), nbr) = v
                        requested_amounts(tot_requesting_nbrs(nbr), nbr) = RR(v)-base_resource(v)
                    enddo
                    ! Count the total number of requests sent over the lifetime of a vertex 
                    IF (t <= reqoff_count .or. reqoff_count == -1) THEN
                        tot_requests_sent_lifetime(v) = tot_requests_sent_lifetime(v) + deg_in_temp(v)
                    ENDIF
                ! Record whether the vertex will die because of money deficiency
                ELSEIF (base_resource(v) < RR(v) .and. money(v) < unit_resource_price*(RR(v)-base_resource(v))) THEN
                    money_deficient_verts = money_deficient_verts + 1
                    money_deficient_verts_t = money_deficient_verts_t + 1
                    money_deficiency(v) = 1
                ENDIF 
            Enddo request_loop

            ! Decide the priority queue
            priority_queue: Do i = 1, tot_vert
                v = vert_list(i)
                IF (base_resource(v) >= RR(v)) THEN
                    If (strategy == "random") Then
                        if (weighted .eqv. .True.) then
                            do j = 1, tot_requesting_nbrs(v)   
                                ! Find the index "k" of the j'th requesting nbr in the adjacency list
                                nbr = requesting_nbrs(j, v)
                                do k = 1, deg_out_temp(v)
                                    if (adj_list_out(k, v) == nbr) then
                                        exit
                                    endif
                                enddo
                                prob(j) = weights_adj_list_out(k, v)
                            enddo
                            prob(1:tot_requesting_nbrs(v)) = prob(1:tot_requesting_nbrs(v)) / sum(prob(1:tot_requesting_nbrs(v)))
                        else
                            prob(1:tot_requesting_nbrs(v)) = 1.0/tot_requesting_nbrs(v)
                        endif
                        ! Randomly shuffle the amounts
                        call random_shuffle(requested_amounts(1:tot_requesting_nbrs(v), v), &
                            & shuffled_indices=shuffled_indices(1:tot_requesting_nbrs(v)), prob=prob(1:tot_requesting_nbrs(v)))
                        ! Now shuffle the list of neighbours according to these shuffled amounts
                        call random_shuffle(requesting_nbrs(1:tot_requesting_nbrs(v), v), & 
                            & shuffled_indices(1:tot_requesting_nbrs(v)), & 
                            & shuffling_indices=shuffled_indices(1:tot_requesting_nbrs(v)))
                    Elseif (strategy == "low_to_high" .and. tot_requesting_nbrs(v) > 0) Then
                        ! Sort the amounts from low to high
                        call range_array(sorting_indices, int(1, 8), tot_requesting_nbrs(v))
                        call mergesort(tot_requesting_nbrs(v), requested_amounts(1:tot_requesting_nbrs(v), v), &
                            & sorting_indices(1:tot_requesting_nbrs(v)))
                        ! Sort the neighbours based on requested amounts
                        call random_shuffle(requesting_nbrs(1:tot_requesting_nbrs(v), v), &
                            & shuffled_indices(1:tot_requesting_nbrs(v)), &
                            & shuffling_indices=sorting_indices(1:tot_requesting_nbrs(v)))
                    Elseif (strategy == "high_to_low" .and. tot_requesting_nbrs(v) > 0) Then
                        ! Sort the amounts from high to low
                        call range_array(sorting_indices, int(1, 8), tot_requesting_nbrs(v))
                        call mergesort(tot_requesting_nbrs(v), requested_amounts(1:tot_requesting_nbrs(v), v), & 
                            & sorting_indices(1:tot_requesting_nbrs(v)))
                        requested_amounts(1:tot_requesting_nbrs(v), v) = requested_amounts(tot_requesting_nbrs(v):1:-1, v)
                        sorting_indices(1:tot_requesting_nbrs(v)) = sorting_indices(tot_requesting_nbrs(v):1:-1)
                        ! Sort the neighbours based on requested amounts
                        call random_shuffle(requesting_nbrs(1:tot_requesting_nbrs(v), v), & 
                            & shuffled_indices(1:tot_requesting_nbrs(v)), &
                            & shuffling_indices=sorting_indices(1:tot_requesting_nbrs(v)))
                    Elseif (strategy == "prop_to_req" .and. tot_requesting_nbrs(v) > 0) Then
                        Do j = 1, tot_requesting_nbrs(v)
                            nbr = requesting_nbrs(j, v)
                            ! Find the index k of the nbr in the adjacency list of v
                            do k = 1, deg_out_temp(v)
                                if (adj_list_out(k, v) == nbr) then
                                    exit 
                                endif
                            enddo
                            prob(j) = weights_adj_list_out(k, v) * requested_amounts(j,v)
                        Enddo
                        norm = sum(prob(1:tot_requesting_nbrs(v)))
                        prob(1:tot_requesting_nbrs(v)) = prob(1:tot_requesting_nbrs(v)) / norm
                        ! Find the indices which would choose the neighbours according to the specified probabilities
                        call random_choice(requested_amounts(1:tot_requesting_nbrs(v), v), & 
                            & dummy_array(1:tot_requesting_nbrs(v)), &
                            & prob=prob(1:tot_requesting_nbrs(v)), &
                            & replace=.False., &
                            & chosen_indices=chosen_indices(1:tot_requesting_nbrs(v)), & 
                            & state=state)
                        ! Shuffled amounts
                        requested_amounts(1:tot_requesting_nbrs(v), v) = dummy_array(1:tot_requesting_nbrs(v))
                        ! Now shuffle the neighbours according to these indices
                        call random_shuffle(requesting_nbrs(1:tot_requesting_nbrs(v), v), &
                            & shuffled_indices(1:tot_requesting_nbrs(v)), & 
                            & shuffling_indices=chosen_indices(1:tot_requesting_nbrs(v)))
                    Elseif (strategy == "prop_to_req_deg" .and. tot_requesting_nbrs(v) > 0) Then
                        Do j = 1, tot_requesting_nbrs(v)
                            nbr = requesting_nbrs(j, v)
                            ! Find the index k of the nbr in the adjacency list of v
                            do k = 1, deg_out_temp(v)
                                if (adj_list_out(k, v) == nbr) then
                                    exit 
                                endif
                            enddo
                            prob(j) = weights_adj_list_out(k, v) * requested_amounts(j,v)/deg_in_temp(nbr)**eta(v)
                        Enddo
                        ! Choose neighbours using their degree and requirements
                        norm = sum(prob(1:tot_requesting_nbrs(v)))
                        prob(1:tot_requesting_nbrs(v)) = prob(1:tot_requesting_nbrs(v)) / norm
                        ! Find the indices which would choose the neighbours according to the specified probabilities
                        call random_choice(requested_amounts(1:tot_requesting_nbrs(v), v), & 
                            & dummy_array(1:tot_requesting_nbrs(v)), &
                            & prob(1:tot_requesting_nbrs(v)), replace=.False., &
                            & chosen_indices=chosen_indices(1:tot_requesting_nbrs(v)), state=state)
                        ! Shuffled amounts
                        requested_amounts(1:tot_requesting_nbrs(v), v) = dummy_array(1:tot_requesting_nbrs(v))
                        ! Now shuffle the neighbours according to these indices
                        call random_shuffle(requesting_nbrs(1:tot_requesting_nbrs(v), v), & 
                            & shuffled_indices(1:tot_requesting_nbrs(v)), & 
                            & shuffling_indices=chosen_indices(1:tot_requesting_nbrs(v)))
                    Endif
                ENDIF
            Enddo priority_queue

            ! Send out offers
            tot_offers_received = 0
            ! The arrays offering_nbrs and offered_amounts need not be re-initialized
            ! In fact, initializing them significantly slows down the code
            surplus = 0
            offer_loop: Do i = 1, tot_vert
                v = vert_list(i)
                IF (base_resource(v) > RR(v)) THEN
                    surplus(v) = base_resource(v) - RR(v)
                    do j = 1, tot_requesting_nbrs(v)
                        nbr = requesting_nbrs(j, v)
                        If (surplus(v) > requested_amounts(j, v) .and. requested_amounts(j, v) > 0) Then
                            tot_offers_received(nbr) = tot_offers_received(nbr) + 1
                            offering_nbrs(tot_offers_received(nbr), nbr) = v
                            offered_amounts(tot_offers_received(nbr), nbr) = requested_amounts(j, v)
                            surplus(v) = surplus(v) - requested_amounts(j, v)
                        Endif
                    enddo
                    ! wastage
                    resource_wastage(v) = resource_wastage(v) + surplus(v)
                ENDIF
            Enddo offer_loop

            ! Accept the offers
            accept_loop: Do i = 1, tot_vert
                v = vert_list(i)
                IF (tot_offers_received(v) > 0) THEN
                    call random_int(int(1,8), tot_offers_received(v), j)
                    nbr = offering_nbrs(j, v) ! This is the neighbour whose offer is accepted
                    ! wastage
                    do k = 1, tot_offers_received(v)
                        If (offering_nbrs(k, v) /= nbr) then
                            resource_wastage(offering_nbrs(k, v)) = resource_wastage(offering_nbrs(k, v)) + offered_amounts(k, v)
                        Endif
                    enddo
                    temp_resource(v) = temp_resource(v) + offered_amounts(j, v)
                    ! Subtract this offered amount from the base resource of the neighbour
                    base_resource(nbr) = base_resource(nbr) -  offered_amounts(j,v)
                    money(v) = money(v) - unit_resource_price * offered_amounts(j, v)
                    money(nbr) = money(nbr) + unit_resource_price * offered_amounts(j, v)
                    ! Update the number of times the vertex v is saved by its neighbours
                    tot_times_saved(v) = tot_times_saved(v) + 1
                ENDIF
                ! Count the total number of offers received over the lifetime of a vertex
                IF (t <= reqoff_count .or. reqoff_count == -1) THEN
                    tot_offers_received_lifetime(v) = tot_offers_received_lifetime(v) + tot_offers_received(v)
                ENDIF
            Enddo accept_loop

            ! Update the base resources
            update_loop: Do i = 1, tot_vert
                v = vert_list(i)
                base_resource(v) = base_resource(v) + temp_resource(v)
            Enddo update_loop

            IF (death .eqv. .True.) THEN
                ! Delete the vulnerable vertices
                delete_loop: Do i = 1, tot_vert
                    v = vert_list(i)
                    IF (base_resource(v) < RR(v)) THEN
                        ! Update the wasted resource amount per unit time
                        tot_resource_wastage = tot_resource_wastage + resource_wastage(v) / t
                        ! If the graph is weighted, compute the total temp degree weights
                        If (weighted .eqv. .True.) Then
                            do j = 1, deg_out_temp(v)
                                deg_out_temp_weights(v) = deg_out_temp_weights(v) + weights_adj_list_out(j, v)
                            enddo
                            do j = 1, deg_in_temp(v)
                                deg_in_temp_weights(v)  = deg_in_temp_weights(v)  + weights_adj_list_in(j, v)
                            enddo
                        Endif
                        If (vertex_data .eqv. .True.) Then
                            ! note down the data of the deleted vertex 
                            If (weighted .eqv. .False.) Then
                                write(2, '(10(I0,A),2(F0.4,A),F0.4)')v-1, ",", deg_out(v), ",", deg_out_temp(v), ",", &
                                    & deg_in(v), ",", deg_in_temp(v), ",", t, "," , tot_requests_sent_lifetime(v), ",", &
                                    & tot_offers_received_lifetime(v), ",", money_deficiency(v), ",", tot_times_saved(v), ",", &
                                    & init_money(v), ",", money(v), ",", resource_wastage(v)
                            Else
                                write(2, '(I0,A,4(F0.4,A),5(I0,A),2(F0.4,A),F0.4)')v-1, ",", deg_out_weights(v), ",", &
                                    & deg_out_temp_weights(v), ",", deg_in_weights(v), ",", deg_in_temp_weights(v), ",", &
                                    & t, "," , tot_requests_sent_lifetime(v), ",", tot_offers_received_lifetime(v), ",", &
                                    & money_deficiency(v), ",", tot_times_saved(v), ",", init_money(v), ",", money(v), ",", &
                                    & resource_wastage(v)
                            Endif
                        Endif
                        ! update the adjacency list(s)
                        directed = .False.
                        call update_adjacency_lists(v, directed, deg_out, deg_out_temp, adj_list_out, &
                            & weights_adj_list_out, weights_adj_list_in, deg_in, deg_in_temp, adj_list_in)
                        vert_t(v) = t
                        ! Update the vertex list
                        update_vert_list: do j = 1, tot_vert
                            If (vert_list(j) == v) Then
                                ! overwrite the entry v by the last element
                                vert_list(j) = vert_list(tot_vert)
                                exit update_vert_list
                            Endif
                        enddo update_vert_list
                        tot_vert = tot_vert - 1
                    ELSEIF (is_vulnerable(v) .eqv. .True.) THEN
                        tot_saved = tot_saved + 1
                    ENDIF
                Enddo delete_loop

            ! death == False, only update vertex states
            ELSE
                tot_resource_wastage = 0
                Do i = 1, tot_vert
                    v = vert_list(i)
                    ! After importing the required resource amount, the base_resource is often
                    ! quite close to RR(v) but can be less than or greater than it within 
                    ! numerical error. Hence we should take the absolute difference. The chances
                    ! of generating base_resource value in a small range around RR(v) before import
                    ! are negligible.
                    If (abs(base_resource(v)-RR(v)) > 1e-8) THEN
                        vert_t(v) = vert_t(v) + 1
                        vertex_active(v) = 1
                        tot_times_saved(v) = tot_times_saved(v) + 1
                        If (is_vulnerable(v) .eqv. .True.) THEN
                            tot_saved = tot_saved + 1
                        Endif
                    Else 
                        vertex_active(v) = 0
                    Endif
                    ! Compute the total resource wastage per unit time
                    tot_resource_wastage = tot_resource_wastage + resource_wastage(v) / t
                Enddo                
                ! Compute the total resource wastage per unit time per unit vertex
                tot_resource_wastage = tot_resource_wastage / n
                If (t == tot_iter) Then
                    ! note down vertex data 
                    If (vertex_data .eqv. .True.) Then
                        Do v = 1, n
                            If (weighted .eqv. .False.) Then
                                write(2, '(7(I0,A),2(F0.4,A),F0.4)')v-1, ",", deg_out(v), ",", deg_in(v), ",",  &
                                    & tot_requests_sent_lifetime(v), ",", tot_offers_received_lifetime(v), ",", &
                                    & money_deficiency(v), ",", tot_times_saved(v), ",", init_money(v), ",", money(v), ",", &
                                    & resource_wastage(v)
                            Else
                                write(2, '(I0,A,2(F0.4,A),4(I0,A),2(F0.4,A),F0.4)')v-1, ",", deg_out_weights(v), ",", &
                                    & deg_in_weights(v), "," , tot_requests_sent_lifetime(v), ",", &
                                    & tot_offers_received_lifetime(v), ",", money_deficiency(v), ",", tot_times_saved(v), ",", &
                                    & init_money(v), ",", money(v), ",", resource_wastage(v)
                            Endif
                        Enddo
                    Endif
                Endif
            ENDIF
            
            ! Write time series of vertices
            IF (vertex_time_series .eqv. .True.) THEN
                If (death .eqv. .False.) Then
                    write(fmt, '(A,I0,A)')"(", n-1, "(I0,','), I0)"
                    write(4,fmt) vertex_active
                Else 
                    write(fmt, '(A,I0,A)')"(", n-1, "(F0.3,','), F0.3)"
                    write(4,fmt) base_resource
                Endif
            ENDIF

            ! Increase the time index
            t = t + 1
            
        ENDDO time_loop
        ! Compute average resource wastage per unit time
        tot_resource_wastage = tot_resource_wastage / n

        deallocate(requesting_nbrs, requested_amounts)
        deallocate(offering_nbrs, offered_amounts)
        deallocate(shuffled_indices, sorting_indices, chosen_indices)
        deallocate(dummy_array, prob)
        IF (temporal .eqv. .True.)then
            close(1)
            close(2)
        ENDIF
        close(3)
    end subroutine request_offer_model
!================================================================================
end module request_offer
