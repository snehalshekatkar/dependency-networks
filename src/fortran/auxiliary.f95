!    This file is part of the package dependency-networks
!
!    Copyright (C) 2020-2021 Snehal Madhukar Shekatkar <snehal@inferred.in>
!
!    This package is free software: you can redistribute it and/or modify
!    it under the terms of the GNU General Public License as published by
!    the Free Software Foundation, either version 3 of the License, or
!    (at your option) any later version.
!
!    The package is distributed in the hope that it will be useful,
!    but WITHOUT ANY WARRANTY; without even the implied warranty of
!    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
!    GNU General Public License for more details.
!
!    You should have received a copy of the GNU General Public License
!    along with this program. If not, see <https://www.gnu.org/licenses/>.

module auxiliary

    implicit none
    contains

    !-----------------------------------------------------------------------

    subroutine get_mask_vert(n, nbr, tot_params, base_resource, RR, beta, eta, deg_in, deg_temp_in, &
        & deg_out, deg_temp_out, ext_prop_vals, mask_array, mask_prop_vals, OR, mask_vert_out)

    ! Get a vertex mask depending upon values of various quantities

    implicit none
    logical :: OR, mask_vert_out
    integer, parameter :: int10 = selected_int_kind(10), real10 = selected_real_kind(10, 10)
    integer (kind=int10) :: i, j, mask_vert, n, nbr, deg_in, deg_temp_in, deg_out, deg_temp_out
    integer (kind=int10) :: tot_params, mask_array(:, :)
    real (kind=real10) :: base_resource, RR, beta(:), eta, ext_prop_vals
    real (kind=real10):: prop_vals(8+tot_params), mask_prop_vals(:)

    prop_vals(1) = base_resource
    prop_vals(2) = RR
    prop_vals(3) = eta
    prop_vals(4) = deg_in
    prop_vals(5) = deg_temp_in
    prop_vals(6) = deg_out
    prop_vals(7) = deg_temp_out
    prop_vals(8) = ext_prop_vals

    DO i = 1, tot_params
        prop_vals(8+i) = beta(i)
    ENDDO
    
    mask_vert = 0
    ! j varies over various properties, mask_array(1, j) == 1 iff 
    ! property number j is to be used, mask_array(2, j) encodes one of 
    ! the comparison operators (<, >, <=, >=, ==, !=, in that order), while
    ! mask_prop_vals(j) contains the value to be used in the masking condition.
    DO j = 1, 8 + tot_params 
        if (mask_array(1, j) > 0.) then
            if (mask_array(2, j) .eq. 0) then
                if (prop_vals(j) < mask_prop_vals(j)) then
                   mask_vert = mask_vert + 1
                endif
            elseif (mask_array(2, j) .eq. 1) then
                if (prop_vals(j) > mask_prop_vals(j)) then
                   mask_vert = mask_vert + 1
                endif
            elseif (mask_array(2, j) .eq. 2) then
                if (prop_vals(j) <= mask_prop_vals(j)) then
                   mask_vert = mask_vert + 1
                endif
            elseif (mask_array(2, j) .eq. 3) then
                if (prop_vals(j) >= mask_prop_vals(j)) then
                   mask_vert = mask_vert + 1
                endif
            elseif (mask_array(2, j) .eq. 4) then
                if (prop_vals(j) == mask_prop_vals(j)) then
                   mask_vert = mask_vert + 1
                endif
            elseif (mask_array(2, j) .eq. 5) then
                if (prop_vals(j) /= mask_prop_vals(j)) then
                   mask_vert = mask_vert + 1
                endif
            endif
        endif
    ENDDO

    if (OR .eqv. .True.) then
        if (mask_vert > 0) then
            mask_vert_out = .True.
        else
            !print*, "delta", prop_vals(1), mask_prop_vals(1)
            !print'(10(I0,1X))', mask_array(1,:)
            !print'(10(I0,1X))', mask_array(2,:)
            !print'(10(F0.3,1X))', mask_prop_vals
            mask_vert_out = .False.
        endif
    else
        if (mask_vert < sum(mask_array(1, :))) then
            mask_vert_out = .False.
        else
            mask_vert_out = .True.
        endif
    endif
    !print*, "mask_nbr is: ", mask_vert_out, base_resource
        
    end subroutine get_mask_vert
!==========================================================================================
end module auxiliary
