!    This file is part of the package dependency-networks
!
!    Copyright (C) 2020-2021 Snehal Madhukar Shekatkar <snehal@inferred.in>
!
!    This package is free software: you can redistribute it and/or modify
!    it under the terms of the GNU General Public License as published by
!    the Free Software Foundation, either version 3 of the License, or
!    (at your option) any later version.
!
!    The package is distributed in the hope that it will be useful,
!    but WITHOUT ANY WARRANTY; without even the implied warranty of
!    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
!    GNU General Public License for more details.
!
!    You should have received a copy of the GNU General Public License
!    along with this program. If not, see <https://www.gnu.org/licenses/>.

module topology

    implicit none
    private :: int10, real10
    integer, parameter :: int10 = selected_int_kind(10), real10 = selected_real_kind(10, 10)

    contains

    subroutine BFS(n, max_deg, adj_list, deg, component, n_vert, n_edges)

    ! Breadth-first-search algorithm to find all the components in a network

    implicit none
    integer (kind=int10) :: i, n, num_vert, num_edges, max_deg, read_ptr, write_ptr
    integer (kind=int10) :: current_vertex, nbr
    integer (kind=int10), intent(out) :: n_vert(n), n_edges(n)
    integer (kind=int10), intent(in) :: adj_list(max_deg, n)
    integer (kind=int10) :: deg(n)
    integer (kind=int10), intent(out) :: component(n)
    integer (kind=int10) :: comp_number, dist(n), vert_seq(n)
    ! Run BFS
    component = -1
    comp_number = 0
    
    DO WHILE (any(component == -1))
        dist = -1
        read_ptr = 1
        write_ptr = 2
        vert_seq = -1
    
        ! Choose current_vertex as the first vertex for which 
        ! the component is unknown
        Do i = 1, n
            if (component(i) == -1)then
                current_vertex = i
                dist(current_vertex) = 0
                comp_number = comp_number + 1
                component(current_vertex) = comp_number
                vert_seq(read_ptr) = current_vertex
                num_vert = 1
                num_edges = 0
                exit
            endif
        Enddo
    
        DO while (read_ptr < write_ptr)
            
            ! Assign distances to the neighours
            Do i = 1, deg(current_vertex)
                nbr = adj_list(i, current_vertex)
                ! number of edges must be updated even when the distance 
                ! is alredy known. This also means that each edge is 
                ! counted twice
                num_edges = num_edges + 1
                if (dist(nbr) == -1)then
                    dist(nbr) = dist(current_vertex) + 1
                    component(nbr) = component(current_vertex)
                    num_vert = num_vert + 1
                    vert_seq(write_ptr) = nbr
                    write_ptr = write_ptr + 1
                endif
            Enddo
    
            read_ptr = read_ptr + 1
            current_vertex = vert_seq(read_ptr)
        ENDDO
        ! Save the number of vertices and edges in the current component
        Do i = 1, n
            if (component(i) == comp_number)then
                n_vert(i) = num_vert
                n_edges(i) = num_edges/2 ! because each edge counted twice
            endif
        Enddo
    ENDDO

    end subroutine BFS
end module topology
