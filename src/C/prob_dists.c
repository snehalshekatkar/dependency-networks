#include <gsl/gsl_rng.h>
#include <gsl/gsl_randist.h>
#include <math.h>

extern gsl_rng* g_rng;

void uniform_(double* x, double* a, double* b)
{
    double r = gsl_ran_flat(g_rng, *a, *b);
    *x = r;
}

void exponential_(double* x, double* beta)
{
    double r = gsl_ran_exponential(g_rng, *beta);
    *x = r;
}

void gaussian_(double* x, double* mu, double* sigma)
{
    double r = gsl_ran_gaussian(g_rng, *sigma);
    *x = *mu + r;
}

void gaussian_positive_(double* x, double* mu, double* sigma)
{
    double r = gsl_ran_gaussian(g_rng, *sigma);
    *x = *mu + r;
    while (*x < 0)
    {
        r = gsl_ran_gaussian(g_rng, *sigma);
        *x = *mu + r;
    }
}

void pareto_(double* x, double* a, double* b)
{
    double r = gsl_ran_pareto(g_rng, *a, *b);
    *x = r;
}

void chi_(double* x, double* k)
{
    // Chi distribution with k degrees of freedom
    double r = gsl_ran_chisq(g_rng, *k);
    *x = sqrt(r);
}

void chi_squared_(double* x, double* k)
{
    // Chi-squared distribution with k degrees of freedom
    double r = gsl_ran_chisq(g_rng, *k);
    *x = r;
}

void logistic_(double* x, double* mu, double* a)
{
    // Logistic distribution with location parameter mu and scale parameter a
    double r = gsl_ran_logistic(g_rng, *a);
    *x = *mu + r;
}

void logistic_positive_(double* x, double* mu, double* a)
{
    // Logistic distribution with location parameter mu and scale parameter a
    double r = gsl_ran_logistic(g_rng, *a);
    *x = *mu + r;
    while (*x < 0)
    {
        r = gsl_ran_logistic(g_rng, *a);
        *x = *mu + r;
    }
}

void gamma_dist_(double* x, double* a, double* theta)
{
    // Gamma distribution with shape parameter a and scale parameter theta
    double r = gsl_ran_gamma(g_rng, *a, *theta);
    *x = r;
}
