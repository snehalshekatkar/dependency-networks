#include <gsl/gsl_rng.h>

// We need a global pointer here. You won't see this everyday.
gsl_rng *g_rng;

void initialize_rng_(long int *seed)
{
    /* Create a random number generator of type RANLUX
     * (F. James, "RANLUX: A Fortran implementation of the high-quality 
     * pseudo-random number generator of Luscher", Computer Physics 
     * Communications, 79 (1994) 111-114)
     */
    g_rng = gsl_rng_alloc(gsl_rng_ranlxs2);
    gsl_rng_set(g_rng, *seed);
}

void free_rng_(long int *t)
{
    gsl_rng_free(g_rng);
}

