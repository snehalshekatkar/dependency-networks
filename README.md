dependency-networks -- python package to analyze dependency networks
======================================================================

dependency-networks is a python package that can be used to study 
dependency networks i.e. the collection of entities that depend on 
each other for various reasons. The package is written in python, C,
and Fortran to take advantage of all three languages. Because of this, 
the end user can use it as if it were a pure python package, although
crucial calculations are performed under-the-hood in C and Fortran. 

The package is tested extensively on GNU/Linux systems, but probably it won't
compile on defective operating systems like Windows or MacOS although we 
haven't tested this. 

dependency-networks is free software, you can redistribute it and/or modify 
it under the terms of the GNU General Public License, version 3 or
above. See COPYING for details.

This program is distributed in the hope that it will be useful, but
WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.

INSTALLATION
------------
Internally, dependency-networks uses numpy, and so numpy needs to be installed.
Similarly, a fortran compiler like gfortran needs to be installed. The package 
relies on GNU Scientific Library (https://www.gnu.org/software/gsl/) for random
number generation, and hence GSL needs to be installed in the standard path on
GNU/Linux systems (/usr/local/include). The random number generator used in the 
package is RANLUX algorithm of Luscher (gsl_rng_ranlxs2) whose period is about
10^171. The package only accepts graphs as networkx or graph-tool's network 
instances, and hence one of these needs to be installed depending upon your 
choice. See the documentations for these. Finally, setuptools needs to be 
installed for the actual installation to work. 

To install the package from the source, follow the instructions below:

1. Download or git-clone this repository to your machine

   git clone https://gitlab.com/snehalshekatkar/dependency-networks.git

2. Run the following command in the terminal:

    ```bash 
    sudo python3 -m pip install dependency-networks/

    ```

    (pay attention to "/" at the end, it is required)

    If you are only upgrading an already existing package (e.g. by pulling
    from the remote), then use:

    ```bash

    sudo python3 -m pip install dependency-networks/ --upgrade

    ```
If you encounter the error:

    ```bash

    ImportError: libgsl.so.27: cannot open shared object file: No such file or directory

    ```

    then add the following to your `.bashrc` file:
    ```bash

    export LD_LIBRARY_PATH=/usr/local/lib:$LD_LIBRARY_PATH

    ```

    and after saving it, source it:

    ```bash

    source ~/.bashrc

    ```

Now the package can be imported in python scripts and the interpreter:

    >>> import dependency_networks as dn
    >>> dn.network_activity(n, g, death=True)


To uninstall the package, run the following command in the terminal:

    sudo pip3 uninstall dependency-networks

--
Snehal Madhukar Shekatkar <snehal@inferred.in>
